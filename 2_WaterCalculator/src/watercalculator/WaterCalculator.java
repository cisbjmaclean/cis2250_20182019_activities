package watercalculator;

import java.util.Scanner;

public class WaterCalculator {

    /**
     * This method will calculate the time to fill a cylander to a certain height.
     * Note:  You are not expected to know how to do this starting the year but 
     * we will be getting into how to do this as the course starts.
     * 
     * @since 20170907
     * @author BJM
     */
    
    public static void main(String[] args) {
//        Calculator theCalculator = new Calculator();        
//        theCalculator.getInput();
//        theCalculator.display();

        Calculator theCalculator2 = new Calculator(11,2,30);
        theCalculator2.display();
        
    }
    
}
