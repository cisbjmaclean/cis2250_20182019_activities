package cis2250.hccis.info.courtbooking.utility;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import cis2250.hccis.info.courtbooking.bo.Booking;

/**
 * Created by Keith Goddard on 2018-01-27.
 */

public class DatabaseHelper extends SQLiteOpenHelper {

    public static final String DATABASE_NAME = "courtbooking.db";
    public static final String TABLE_BOOKING = "booking";
    public static final String BOOKING_ID = "bookingId";
    public static final String BOOKING_USERID = "userId";
    public static final String BOOKING_COURTID = "courtId";
    public static final String BOOKING_OPPONENT = "opponent";
    public static final String BOOKING_DATE = "bookingDate";
    public static final String BOOKING_START_TIME = "bookingStartTime";
    public static final String BOOKING_CREATED_DATETIME = "createdDateTime";
    public static final String BOOKING_CREATED_USERID = "createdUserId";
    public static final String BOOKING_UPDATED_DATETIME = "updatedDateTime";
    public static final String BOOKING_UPDATED_USERID = "updatedUserId";


    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, 3);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        Log.d("KGDB", "DatabaseHelper onCreate called");
        String sql = "CREATE TABLE " + TABLE_BOOKING + " (" +
                BOOKING_ID + " INTEGER PRIMARY KEY, " +
                BOOKING_USERID + " INTEGER, " +
                BOOKING_COURTID + " INTEGER, " +
                BOOKING_OPPONENT + " INTEGER, " +
                BOOKING_DATE + " TEXT, " +
                BOOKING_START_TIME + " TEXT, " +
                BOOKING_CREATED_DATETIME + " TEXT, " +
                BOOKING_CREATED_USERID + " TEXT, " +
                BOOKING_UPDATED_DATETIME + " TEXT, " +
                BOOKING_UPDATED_USERID + " TEXT);";
        Log.d("KGDB", "SQL: " + sql);
        db.execSQL(sql);
        Log.d("KGDB", "DatabaseHelper onCreate finished");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {
        Log.d("KGDB", "DatabaseHelper onUpdate called");
        String sql = "DROP TABLE IF EXISTS " + TABLE_BOOKING + ";";
        db.execSQL(sql);
        onCreate(db);
        Log.d("KGDB", "DatabaseHelper onUpdate finished");
    }

    public boolean insertBooking(Booking booking) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(BOOKING_ID, booking.getBookingId());
        contentValues.put(BOOKING_COURTID, booking.getCourtId());
        contentValues.put(BOOKING_USERID, booking.getUserId());
        contentValues.put(BOOKING_OPPONENT, booking.getOpponent());
        contentValues.put(BOOKING_DATE, booking.getBookingDate());
        contentValues.put(BOOKING_START_TIME, booking.getBookingStartTime());
        contentValues.put(BOOKING_CREATED_DATETIME, booking.getCreatedDateTime());
        contentValues.put(BOOKING_CREATED_USERID, booking.getCreatedUserId());
        contentValues.put(BOOKING_UPDATED_DATETIME, booking.getUpdatedDateTime());
        contentValues.put(BOOKING_UPDATED_USERID, booking.getUpdatedUserId());

        long result = db.insert(TABLE_BOOKING, null, contentValues);
        Log.d("KGDB", "Inserting booking ID: " + booking.getBookingId());
        return result != -1;
    }

    public Cursor getAllBookings() {
        Log.d("KGDB", "Get all bookings called");
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor result = db.rawQuery("SELECT * FROM " + TABLE_BOOKING, null);
        Log.d("KGDB", "Get all bookings done");
        Log.d("KGDB", "CURSOR SIZE: " + result.getCount());
        return result;
    }

    public void deleteAllBookings() {
        Log.d("KGDB", "Delete all bookings called");
        SQLiteDatabase db = this.getWritableDatabase();

        db.delete(TABLE_BOOKING,null,null);
        Log.d("KGDB", "DELETED ALL BOOKINGS!!!");
    }
}
