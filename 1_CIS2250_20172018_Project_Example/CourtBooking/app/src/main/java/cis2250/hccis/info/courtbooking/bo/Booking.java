package cis2250.hccis.info.courtbooking.bo;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Comparator;
import java.util.Date;
import java.util.Locale;

/**
 * Created by kgoddard on 2/2/2018.
 */

public class Booking implements Parcelable {
    // {"bookingId":2,"userId":1,"courtId":1,"bookingDate":"01/25/2018","bookingStartTime":"0800","createdDateTime":"Jan 24, 2018 3:17:59 PM","createdUserId":"1","updatedDateTime":"Jan 24, 2018 3:17:59 PM","updatedUserId":"1"}
    int bookingId;
    int userId;
    int courtId;
    int opponent;
    String bookingDate;
    String bookingStartTime;
    String createdDateTime;
    String createdUserId;
    String updatedDateTime;
    String updatedUserId;

    public Booking() {

    }

    protected Booking(Parcel in) {
        bookingId = in.readInt();
        userId = in.readInt();
        courtId = in.readInt();
        opponent = in.readInt();
        bookingDate = in.readString();
        bookingStartTime = in.readString();
        createdDateTime = in.readString();
        createdUserId = in.readString();
        updatedDateTime = in.readString();
        updatedUserId = in.readString();
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(this.bookingId);
        parcel.writeInt(this.userId);
        parcel.writeInt(this.courtId);
        parcel.writeInt(this.opponent);
        parcel.writeString(this.bookingDate);
        parcel.writeString(this.bookingStartTime);
        parcel.writeString(this.createdDateTime);
        parcel.writeString(this.createdUserId);
        parcel.writeString(this.updatedDateTime);
        parcel.writeString(this.updatedUserId);
    }

    public static final Creator<Booking> CREATOR = new Creator<Booking>() {
        @Override
        public Booking createFromParcel(Parcel in) {
            return new Booking(in);
        }

        @Override
        public Booking[] newArray(int size) {
            return new Booking[size];
        }
    };

    public int getOpponent() {
        return opponent;
    }



    public int getBookingId() {
        return bookingId;
    }

    public int getUserId() {
        return userId;
    }

    public int getCourtId() {
        return courtId;
    }

    public String getBookingDate() {
        return bookingDate;
    }

    public String getBookingStartTime() {
        return bookingStartTime;
    }

    public String getCreatedDateTime() {
        return createdDateTime;
    }

    public String getCreatedUserId() {
        return createdUserId;
    }

    public String getUpdatedDateTime() {
        return updatedDateTime;
    }

    public String getUpdatedUserId() {
        return updatedUserId;
    }

    public Booking(int bookingId, int userId, int courtId, int opponent, String bookingDate, String bookingStartTime, String createdDateTime, String createdUserId, String updatedDateTime, String updatedUserId) {
        this.bookingId = bookingId;
        this.userId = userId;
        this.courtId = courtId;
        this.opponent = opponent;
        this.bookingDate = bookingDate;
        this.bookingStartTime = bookingStartTime;
        this.createdDateTime = createdDateTime;
        this.createdUserId = createdUserId;
        this.updatedDateTime = updatedDateTime;
        this.updatedUserId = updatedUserId;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public String basicToString() {
        return "Booking Date: " +bookingDate + "\nStart Time: " + bookingStartTime + "\nCourt ID: " + courtId;
    }

    public static class BookingDateComparator implements Comparator<Booking> {

        @Override
        public int compare(Booking booking, Booking t1) {
            SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy hhmm", Locale.CANADA);
            Date firstDate;
            Date secondDate;
            try {
                firstDate = sdf.parse(booking.getBookingDate() + " " + booking.getBookingStartTime());
                secondDate = sdf.parse(t1.getBookingDate() + " " + t1.getBookingStartTime());
                return firstDate.compareTo(secondDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }

            return 0; //if the parses fail then return 0 (probably not the best, but it'll work)
        }
    }
}
