package cis2250.hccis.info.courtbooking.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import cis2250.hccis.info.courtbooking.R;
import cis2250.hccis.info.courtbooking.fragment.AboutFragment;
import cis2250.hccis.info.courtbooking.fragment.BookingsFragment;
import cis2250.hccis.info.courtbooking.fragment.HelpFragment;
import cis2250.hccis.info.courtbooking.fragment.HomeFragment;
import cis2250.hccis.info.courtbooking.fragment.LoginFragment;
import cis2250.hccis.info.courtbooking.fragment.SearchFragment;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, HomeFragment.OnFragmentInteractionListener, BookingsFragment.OnFragmentInteractionListener, SearchFragment.OnFragmentInteractionListener, LoginFragment.OnFragmentInteractionListener, AboutFragment.OnFragmentInteractionListener, HelpFragment.OnFragmentInteractionListener {

    FragmentManager fm = this.getFragmentManager();
    FragmentTransaction ft = fm.beginTransaction();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        //******************************************************************************************
        // Set the initial fragment on the main activity layout
        //******************************************************************************************

        if (savedInstanceState == null) {
            ft.replace(R.id.fragment, new HomeFragment());
            ft.commit();
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    /**
     * This method can be used to show a dialog in your activity.  This will allow the user to
     * enter a positive or negative response.
     * @since 20180116
     * @author BJM/CIS2250
     * @param activity
     * @param title
     * @param message
     */

    public void showDialog(Activity activity, String title, CharSequence message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);

        if (title != null) builder.setTitle(title);

        builder.setMessage(message);
        builder.setPositiveButton("Sure", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                Toast.makeText(getApplicationContext(), "selected-positive", Toast.LENGTH_LONG).show();
            }
        });
        builder.setNegativeButton("Not Really", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                Toast.makeText(getApplicationContext(), "selected-negative", Toast.LENGTH_LONG).show();
            }
        });
        builder.show();
    }



    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            if (fm.getBackStackEntryCount() > 0) {
                Log.d("KGTEST", "popping backstack");
                fm.popBackStack();
            } else {
                Log.d("KGTEST", "nothing on backstack, calling super");
                super.onBackPressed();
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);

        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        MenuItem actionLogout = menu.findItem(R.id.action_logout);
        SharedPreferences sharedPref = getPreferences(Context.MODE_PRIVATE);
        int userId = sharedPref.getInt("userId", 0);
        actionLogout.setVisible(userId != 0);
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        Fragment newFragment = new HomeFragment(); //load the home fragment in case this doesn't work

        //noinspection SimplifiableIfStatement
        switch (id) {
            case R.id.action_about: {
                newFragment = new AboutFragment();
                break;
            }
            case R.id.action_help: {
                newFragment = new HelpFragment();
                break;
            }
            case R.id.action_logout: {
                SharedPreferences sharedPref = getPreferences(Context.MODE_PRIVATE);

                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putString("password", "");
                editor.putInt("userId", 0);
                editor.apply();
                Toast.makeText(this, "Logged out", Toast.LENGTH_LONG).show();
            }
        }
        ft = fm.beginTransaction();
        ft.replace(R.id.fragment, newFragment);
        ft.commit();

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        Fragment newFragment = new HomeFragment();
        switch (item.getItemId()) {
            case R.id.nav_home:
                newFragment = new HomeFragment();
                break;
            case R.id.nav_search:
                newFragment = new SearchFragment();
                break;
            case R.id.nav_bookings:
                newFragment = new BookingsFragment();
                break;
        }

        ft = fm.beginTransaction();
        ft.replace(R.id.fragment, newFragment);
        ft.commit();

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);

        return true;
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
