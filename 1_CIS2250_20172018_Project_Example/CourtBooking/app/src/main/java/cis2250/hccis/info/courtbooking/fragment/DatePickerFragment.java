package cis2250.hccis.info.courtbooking.fragment;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.util.Log;
import android.widget.DatePicker;
import android.widget.EditText;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * Created by Keith Goddard on 2018-01-27.
 */

public class DatePickerFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener {
    private final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.CANADA);
    private EditText editText;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Calendar c = Calendar.getInstance();
        if (editText != null) { //if the editText attribute is not null, then parse the String into a Date and use that date as the default
            String dateString = editText.getText().toString();
            try {
                c.setTime(dateFormat.parse(dateString));
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        //get the data out of the calendar instance to set the default
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);
        // Create a new instance of DatePickerDialog and return it
        DatePickerDialog dp = new DatePickerDialog(getActivity(), this, year, month, day);
        //set today as the minimum for the date picker (may need to modify this later if allowing multiple date selects that need to work together)
        dp.getDatePicker().setMinDate(new Date().getTime());
        return dp;
    }


    @Override
    public void onDateSet(DatePicker datePicker, int year, int month, int day) {
        Calendar selectedDate = Calendar.getInstance();
        selectedDate.set(Calendar.YEAR, year);
        selectedDate.set(Calendar.MONTH, month);
        selectedDate.set(Calendar.DAY_OF_MONTH, day);

        String dateString = dateFormat.format(selectedDate.getTime());
        Log.d("KGTEST", "Date String: " + dateString);
        editText.setText(dateString);
    }

    public void setEditText(EditText editText) {
        this.editText = editText;
    }

    public EditText getEditText() {
        return this.editText;
    }
}
