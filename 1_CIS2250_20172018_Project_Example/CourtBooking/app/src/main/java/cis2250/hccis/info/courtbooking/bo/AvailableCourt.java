package cis2250.hccis.info.courtbooking.bo;

/**
 * Created by kgoddard on 2/2/2018.
 */

public class AvailableCourt {
    private int courtId;
    private int courtType;
    private String date;
    private String[] startTimes;
    private String[] endTimes;

    public int getCourtId() {
        return courtId;
    }

    public int getCourtType() {
        return courtType;
    }

    public String getDate() {
        return date;
    }

    public String[] getStartTimes() {
        return startTimes;
    }

    public String[] getEndTimes() {
        return endTimes;
    }

    @Override
    public String toString() {
        return "Court ID: " + courtId + ", Date: " + date;
    }
}
