package cis2250.hccis.info.courtbooking.fragment;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Fragment;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import cis2250.hccis.info.courtbooking.R;
import cis2250.hccis.info.courtbooking.bo.User;
import cis2250.hccis.info.courtbooking.utility.Utility;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link LoginFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link LoginFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class LoginFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public LoginFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment LoginFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static LoginFragment newInstance(String param1, String param2) {
        LoginFragment fragment = new LoginFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (savedInstanceState != null) {
            EditText editTextUsername = getActivity().findViewById(R.id.editTextUsername);
            EditText editTextPassword = getActivity().findViewById(R.id.editTextPassword);
            editTextUsername.setText(savedInstanceState.getString("username"));
            editTextPassword.setText(savedInstanceState.getString("password"));
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_login, container, false);
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onStart() {
        super.onStart();
        EditText editTextUsername = getActivity().findViewById(R.id.editTextUsername);
        EditText editTextPassword = getActivity().findViewById(R.id.editTextPassword);
        if (editTextUsername.getText().toString().isEmpty()) { //if the username is empty (it was not loaded from the savedInstanceState)
            SharedPreferences sharedPref = getActivity().getPreferences(Context.MODE_PRIVATE);
            //attempt to get the username and password out of the sharedPref
            editTextUsername.setText(sharedPref.getString("username", ""));
            //if the remember login button was checked then get the password from storage
            if (sharedPref.getBoolean("rememberLogin", false)) {
                editTextPassword.setText(sharedPref.getString("password", ""));
            }
            //if the username and password are both not empty then submit them and attempt to authenticate
            if (!(editTextPassword.getText().toString().isEmpty() || editTextUsername.getText().toString().isEmpty())) {
                new HttpRequestTask().execute();
            }
        }
        Button buttonLogin = getActivity().findViewById(R.id.buttonLogin);
        buttonLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new HttpRequestTask().execute();
            }
        });
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        EditText editTextUsername = getActivity().findViewById(R.id.editTextUsername);
        EditText editTextPassword = getActivity().findViewById(R.id.editTextPassword);

        outState.putString("username", editTextUsername.getText().toString());
        outState.putString("password", editTextPassword.getText().toString());
    }

    private class HttpRequestTask extends AsyncTask<Void, Void, User> {
        @Override
        protected User doInBackground(Void... params) {
            try {
                EditText editTextUsername = getActivity().findViewById(R.id.editTextUsername);
                EditText editTextPassword = getActivity().findViewById(R.id.editTextPassword);
                String username = editTextUsername.getText().toString();
                String unhashedPassword = editTextPassword.getText().toString();
                if (username.isEmpty() || unhashedPassword.isEmpty()) {
                    return null;
                }

                // TEST URL: http://localhost:8080/courtbooking/rest/UserService/user/bmaclean/202cb962ac59075b964b07152d234b70

//                final String url = "http://10.0.2.2:8080/courtbooking/rest/UserService/user/" + username + "/" + md5(unhashedPassword);
                final String url = "http://hccis.info:8080/courtbooking/rest/UserService/user/" + username + "/" + Utility.md5(unhashedPassword);
                Log.d("fgotell_TEST", "doInBackground: " + url);
                RestTemplate restTemplate = new RestTemplate();
                restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
                User user = restTemplate.getForObject(url, User.class);
                Log.d("KGTEST", "User: " + user.toString());
                return user;
            } catch (Exception e) {
                Log.e("MainActivity", e.getMessage(), e);
            }

            return null;
        }

        protected void onPostExecute(User user) {
            //if the returned User is not null or invalid (type code == 0) then display the data
            if (user != null && !user.getUserTypeCode().equals("0")) {
                Toast.makeText(getActivity(), "Welcome, " + user.getFirstName(), Toast.LENGTH_LONG).show();
                SharedPreferences sharedPref = getActivity().getPreferences(Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putInt("userId", user.getUserId());
                editor.putString("username", user.getUsername());
                editor.putString("password", user.getPassword());
                editor.apply();
                Log.d("KGTEST", "USER ID in Shared: " + sharedPref.getInt("userId", 0));
                FragmentManager fm = getFragmentManager();
                FragmentTransaction ft = fm.beginTransaction();
                ft.replace(R.id.fragment, new BookingsFragment());
                ft.commit();
            }
        }

    }

}
