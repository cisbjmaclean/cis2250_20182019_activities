package cis2250.hccis.info.courtbooking.adapters;

import android.app.Activity;
import android.app.DialogFragment;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import cis2250.hccis.info.courtbooking.R;
import cis2250.hccis.info.courtbooking.bo.Booking;
import cis2250.hccis.info.courtbooking.fragment.ExpandedBookingFragment;

/**
 * Created by kgoddard on 2/2/2018.
 */

public class BookingAdapter extends RecyclerView.Adapter<BookingAdapter.MyViewHolder> {
    private List<Booking> bookings;
    private Context context;

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.booking_row, parent, false);
        Log.d("KGADAPTER", "onCreateViewHolder");
        return new MyViewHolder(itemView);
    }

    public BookingAdapter(Context context, List<Booking> bookingsList) {
        Log.d("KGADAPTER", "Instantiating adapter");
        this.context = context;
        this.bookings = bookingsList;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        final Booking booking = bookings.get(position);
        String dateString = "Booking Date: " + booking.getBookingDate() + " @ " + booking.getBookingStartTime();
        holder.bookingDate.setText(dateString);
        String courtIdString = "Court ID: " + booking.getCourtId();
        holder.courtId.setText(courtIdString);

        holder.expand.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d("KGTEST", "Expanding booking " + booking.getBookingId());
                // DialogFragment.show() will take care of adding the fragment
                // in a transaction.  We also want to remove any currently showing
                // dialog, so make our own transaction and take care of that here.
                FragmentTransaction ft = ((Activity) context).getFragmentManager().beginTransaction();
                Fragment prev = ((Activity) context).getFragmentManager().findFragmentByTag("dialog");
                if (prev != null) {
                    ft.remove(prev);
                }
                ft.addToBackStack(null);

                // Create and show the dialog.
                DialogFragment newFragment = ExpandedBookingFragment.newInstance(booking);
                newFragment.show(ft, "dialog");
            }
        });
    }

    @Override
    public int getItemCount() {
        return bookings.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView bookingDate, courtId;
        public ImageButton expand;

        public MyViewHolder(View view) {
            super(view);
            Log.d("KGADAPTER", "Instantiating MyViewHolder");
            bookingDate = view.findViewById(R.id.textViewBookingDate);
            courtId = view.findViewById(R.id.textViewBookingCourtId);
            expand = view.findViewById(R.id.imageButtonExpandBooking);
        }

        @Override
        public void onClick(View v) {
            Toast.makeText(v.getContext(), "clicked", Toast.LENGTH_LONG).show();
        }
    }

}
