package cis2250.hccis.info.courtbooking.fragment;

import android.Manifest;
import android.app.DialogFragment;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.CalendarContract;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import cis2250.hccis.info.courtbooking.R;
import cis2250.hccis.info.courtbooking.bo.Booking;

/**
 * Created by kgoddard on 2/6/2018.
 */

public class ExpandedBookingFragment extends DialogFragment {
    Booking booking;

    public static ExpandedBookingFragment newInstance(Booking booking) {
        ExpandedBookingFragment ebf = new ExpandedBookingFragment();
        Bundle args = new Bundle();
        args.putParcelable("booking", booking);
        ebf.setArguments(args);
        return ebf;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        booking = getArguments().getParcelable("booking");
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_expanded_booking, container, false);
        TextView tvBookingDate = v.findViewById(R.id.tvExpandedDate);
        TextView tvStartTime = v.findViewById(R.id.tvExpandedStartTime);
        TextView tvCourtId = v.findViewById(R.id.tvExpandedCourtId);
        TextView tvOpponent = v.findViewById(R.id.tvExpandedOpponent);
        TextView tvUpdated = v.findViewById(R.id.tvExpandedUpdatedDateTime);
        ImageButton ibCalendar = v.findViewById(R.id.ibExpandedAddCalendar);
        ImageButton ibEmail = v.findViewById(R.id.ibExpandedEmail);

        String dateString = "Booking Date: " + booking.getBookingDate();
        String timeString = "Start Time: " + booking.getBookingStartTime();
        String courtString = "Court ID: " + booking.getCourtId();
        String opponentString = "Opponent ID: " + booking.getOpponent();
        String updateString = "Updated Date: " + booking.getUpdatedDateTime();

        tvBookingDate.setText(dateString);
        tvStartTime.setText(timeString);
        tvCourtId.setText(courtString);
        tvOpponent.setText(opponentString);
        tvUpdated.setText(updateString);

        ibCalendar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addBookingToCalendar(booking);
            }
        });
        ibEmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                emailBooking(booking);
            }
        });
        return v;
    }

    private void emailBooking(Booking booking) {
        //Create some stuff to go into an email
        Intent emailIntent = new Intent(Intent.ACTION_SEND);
        emailIntent.setData(Uri.parse("mailto:"));
        emailIntent.setType("text/plain");
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Court booked at the Spa!");
        emailIntent.putExtra(Intent.EXTRA_TEXT, booking.basicToString());

        try {
            startActivity(emailIntent);
        } catch (Exception ex) {
            Toast.makeText(getActivity(), "You do not have any email clients configured at this time.", Toast.LENGTH_LONG).show();
        }
    }

    private void addBookingToCalendar(Booking booking) {
        //Add booking to the Calendar
        long startMilliseconds = 0, endMilliseconds = 0;
        TimeZone timeZone = TimeZone.getTimeZone("America/Halifax"); //time zone for Spa bookings are all America/Halifax
        Calendar start = Calendar.getInstance(timeZone);
        //sample date format = "02/08/2018 0800" = "MM/dd/yyyy hhmm"
        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy hhmm", Locale.CANADA);
        Date date = null;
        try {
            date = sdf.parse(booking.getBookingDate() + " " + booking.getBookingStartTime());
        } catch (Exception ex) {
            Log.d("KGTEST","Failed to parse date.");
            return; //if the date fails to parse then just return
        }
        start.setTime(date);
        startMilliseconds = start.getTimeInMillis();
        endMilliseconds = startMilliseconds + (40 * 60 * 1000); //end time is 40 minutes later -- should be gotten from Schedule service, but this is okay for now



        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_CALENDAR) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            Log.d("KGTEST","Cannot access Calendar, launching intent instead.");
            Intent intent = new Intent(Intent.ACTION_INSERT)
                    .setData(CalendarContract.Events.CONTENT_URI)
                    .putExtra(CalendarContract.EXTRA_EVENT_BEGIN_TIME, startMilliseconds)
                    .putExtra(CalendarContract.EXTRA_EVENT_END_TIME, endMilliseconds)
                    .putExtra(CalendarContract.Events.TITLE, "Court booked at The Spa")
                    .putExtra(CalendarContract.Events.DESCRIPTION, "Court ID " + booking.getCourtId())
                    .putExtra(CalendarContract.Events.EVENT_LOCATION, "Spa Total Fitness")
                    .putExtra(CalendarContract.Events.AVAILABILITY, CalendarContract.Events.AVAILABILITY_BUSY);
            startActivity(intent);
            return;
        }

        //if permissions are available then just insert the calendar event
        ContentResolver cr = getActivity().getContentResolver();
        ContentValues values = new ContentValues();
        values.put(CalendarContract.Events.DTSTART, startMilliseconds);
        values.put(CalendarContract.Events.DTEND, endMilliseconds);
        values.put(CalendarContract.Events.EVENT_TIMEZONE, timeZone.getID());
        values.put(CalendarContract.Events.TITLE, "Court booked at The Spa");
        values.put(CalendarContract.Events.DESCRIPTION,"Court ID " + booking.getCourtId());
        values.put(CalendarContract.Events.CALENDAR_ID, 1);
//        values.put(CalendarContract.Events.ALL_DAY, 1);
        values.put(CalendarContract.Events.HAS_ALARM, 1);
        Uri uri = cr.insert(CalendarContract.Events.CONTENT_URI, values);

        // Retrieve ID for new event
        String eventID = uri.getLastPathSegment();
        Log.d("KGTEST","The event was added with id "+eventID+"in calendar ");
        Toast.makeText(getActivity().getBaseContext(), "Added booking to your calendar!", Toast.LENGTH_LONG)
                .show();
    }
}
