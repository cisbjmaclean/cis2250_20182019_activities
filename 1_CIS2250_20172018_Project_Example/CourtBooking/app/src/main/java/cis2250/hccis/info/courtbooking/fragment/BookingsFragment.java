package cis2250.hccis.info.courtbooking.fragment;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import cis2250.hccis.info.courtbooking.R;
import cis2250.hccis.info.courtbooking.adapters.BookingAdapter;
import cis2250.hccis.info.courtbooking.bo.Booking;
import cis2250.hccis.info.courtbooking.bo.Booking.BookingDateComparator;
import cis2250.hccis.info.courtbooking.utility.DatabaseHelper;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link BookingsFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link BookingsFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class BookingsFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private DatabaseHelper myDB;
    private RecyclerView recyclerView;
    private BookingAdapter bookingAdapter;
    private List<Booking> bookingList = new ArrayList<Booking>();

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public BookingsFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment BookingsFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static BookingsFragment newInstance(String param1, String param2) {
        BookingsFragment fragment = new BookingsFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
        SharedPreferences sharedPref = getActivity().getPreferences(Context.MODE_PRIVATE);
        int userId = sharedPref.getInt("userId", 0);
        Log.d("KGTEST", "User ID: " + userId);
        if (userId == 0) {
            FragmentManager fm = getFragmentManager();
            FragmentTransaction ft = fm.beginTransaction();
            ft.replace(R.id.fragment, new LoginFragment());
            ft.commit();
        }


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_bookings, container, false);
        recyclerView = view.findViewById(R.id.recyclerViewBookings);
        bookingAdapter = new BookingAdapter(getActivity(), bookingList);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL));
        recyclerView.setAdapter(bookingAdapter);
        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    @Override
    public void onStart() {
        super.onStart();
        myDB = new DatabaseHelper(getActivity());
        new HttpRequestTask().execute();
    }

    private class HttpRequestTask extends AsyncTask<Void, Void, Booking[]> {
        @Override
        protected Booking[] doInBackground(Void... params) {
            try {
                SharedPreferences sharedPref = getActivity().getPreferences(Context.MODE_PRIVATE);
                int userId = sharedPref.getInt("userId", 0);
                final String url = "http://hccis.info:8080/courtbooking/rest/BookingService/bookings/" + userId;
                Log.d("KGTEST", "doInBackground: " + url);
                RestTemplate restTemplate = new RestTemplate();
                restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
                Booking[] bookings = null;
                try {
                    bookings = restTemplate.getForObject(url, Booking[].class);
                } catch (Exception e) {
                }

                return bookings;
            } catch (Exception e) {
                Log.e("MainActivity", e.getMessage(), e);
            }

            return null;
        }

        protected void onPostExecute(Booking[] bookings) {
            if (bookings != null && bookings.length != 0) { //if the result is not null or empty
                myDB.deleteAllBookings();
                try {
                    for (Booking booking : bookings) {
                        myDB.insertBooking(booking);
                    }
                } catch (Exception e) {
                    Log.d("KGTEST", "EXCEPTION IN onPostExecute");
                }
            } else {
                Cursor result = myDB.getAllBookings();
                bookings = new Booking[result.getCount()];
                int index = 0;

                while (result.moveToNext()) {
                    int bookingId = result.getInt(0);
                    int userId = result.getInt(1);
                    int courtId = result.getInt(2);
                    int opponent = result.getInt(3);
                    String date = result.getString(4);
                    String startTime = result.getString(5);
                    String createdDateTime = result.getString(6);
                    String createdUserId = result.getString(7);
                    String updatedDateTime = result.getString(8);
                    String updatedUserId = result.getString(9);

                    Booking booking = new Booking(bookingId, userId, courtId, opponent, date, startTime, createdDateTime, createdUserId, updatedDateTime, updatedUserId);
                    bookings[index] = booking;
                    index++;
                }
            }
            Arrays.sort(bookings, new BookingDateComparator());
            bookingList.clear();
            bookingList.addAll(Arrays.asList(bookings));
            bookingAdapter.notifyDataSetChanged();
        }

    }
}
