package cis2250.hccis.info.fragmentapp.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CalendarView;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;

import cis2250.hccis.info.fragmentapp.R;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link FragmentAkmelMeric.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link FragmentAkmelMeric#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FragmentAkmelMeric extends Fragment{
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public FragmentAkmelMeric() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment FragmentAkmelMeric.
     */
    // TODO: Rename and change types and number of parameters
    public static FragmentAkmelMeric newInstance(String param1, String param2) {
        FragmentAkmelMeric fragment = new FragmentAkmelMeric();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_akmel_meric, container, false);
    }

//    // TODO: Rename method, update argument and hook method into UI event
//    public void onButtonPressed(Uri uri) {
//        if (mListener != null) {
//            mListener.onFragmentInteraction(uri);
//        }
//    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(String message);
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    @Override
    public void onStart() {
        super.onStart();

        final TextView txtOutput=getView().findViewById(R.id.txtOutput);
        CalendarView calView =  getView().findViewById(R.id.calendarView);
        SeekBar seekBar = getView().findViewById(R.id.seekBar);
        final ImageView img = (ImageView) getView().findViewById(R.id.imageView2);
        img.setImageResource(R.drawable.stop);
        img.setTag(R.drawable.stop);

        // CalendarView widget implementation
        // When a date is selected from the calendar, selected date is printed in output text
        // in dd/mm/yyyy format

        calView.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {
            @Override
            public void onSelectedDayChange(@NonNull CalendarView calendarView, int i, int i1, int i2) {
                txtOutput.setText(i2+ "/" +(i1+1) + "/" + i);

            }
        });


         // SeekBar widget implementation
         // When a seekbar progress is changed the progress value printed in output text

        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener(){
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                txtOutput.setText(String.valueOf(i));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });


        // ImageView widget implementation
        // When the picture is clicked it changes to the next one

        img.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Integer i= (Integer)v.getTag();
                if(i== R.drawable.go){
                    img.setImageResource(R.drawable.stop);
                    img.setTag(R.drawable.stop);
                }else if(i== R.drawable.wait){
                    img.setImageResource(R.drawable.go);
                    img.setTag(R.drawable.go);
                }else{
                    img.setImageResource(R.drawable.wait);
                    img.setTag(R.drawable.wait);
                }
            }
        });
    }
}
