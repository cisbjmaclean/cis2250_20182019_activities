package cis2250.hccis.info.fragmentapp.fragment;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.Switch;

import cis2250.hccis.info.fragmentapp.R;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link FragmentMorrisonEvan.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link FragmentMorrisonEvan#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FragmentMorrisonEvan extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private ProgressBar progressBar;
    private int progressStatus = 0;
    private Handler handler = new Handler();

    private OnFragmentInteractionListener mListener;

    public FragmentMorrisonEvan() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment FragmentMorrisonEvan.
     */
    // TODO: Rename and change types and number of parameters
    public static FragmentMorrisonEvan newInstance(String param1, String param2) {
        FragmentMorrisonEvan fragment = new FragmentMorrisonEvan();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    public void onStart() {
        super.onStart();
        final RatingBar rating = getView().findViewById(R.id.ratingBar);
        final Switch switchSleepMode = getView().findViewById(R.id.switchSleepMode);

        rating.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float v, boolean b) {
                Log.d("Evan Test", "Rating = " + rating.getRating());
                if (rating.getNumStars() >= 2.5) {
                    View beastmode = getView().findViewById(R.id.switchSleepMode);
                    beastmode.setVisibility(View.VISIBLE);

                } else if (rating.getNumStars() <= 2) {
                    View beastmode = getView().findViewById(R.id.switchSleepMode);
                    beastmode.setVisibility(View.INVISIBLE);
                }


            }


        });

        switchSleepMode.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
               public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                   Log.d("Radio button", "Raido Button = " + switchSleepMode.isChecked());

                   progressBar = getView().findViewById(R.id.progressBar);
                   progressBar.setVisibility(View.VISIBLE);
                   new Thread(new Runnable() {
                       public void run() {
                           while (progressStatus < 100) {
                               progressBar.setMax(4000);
                               // Update the progress bar and display the
                               //current value in the text view
                               handler.post(new Runnable() {
                                   public void run() {
                                       for (int i=0; i<progressBar.getMax(); i++) {
                                           progressBar.setProgress(i);
                                           progressStatus++;
                                       }
                                   }
                               });
                               try {
                                   // Sleep for 200 milliseconds.
                                   Thread.sleep(50);
                               } catch (InterruptedException e) {
                                   e.printStackTrace();
                               }
                           }
                       }
                   }).start();
//                   if (switchSleepMode.isChecked()) {
//
//
//                       bar = getView().findViewById(R.id.progressBar);
//                       bar.setVisibility(View.VISIBLE);
//                       bar.setMax(4000);
//
//                       for (int i=0; i<bar.getMax(); i++)
//                       {
//                           bar.setProgress(i);
//                           bar.setSecondaryProgress(i + 10);
//                       }
//
//                   }
//                   if (bar.getProgress() >= 999) {
//                       Log.d("Test Progress Bar", "Progress = " + bar.getProgress());
//                   }
               }
           }
        );

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_blank_fragment_evan, container, false);
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onRatingSelection(String message) {
        if (mListener != null) {
            mListener.onFragmentInteraction(message);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name

        void onFragmentInteraction(String message);
    }
}
