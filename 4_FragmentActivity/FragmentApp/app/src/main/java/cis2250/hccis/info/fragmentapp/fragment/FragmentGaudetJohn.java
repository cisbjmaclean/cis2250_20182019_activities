package cis2250.hccis.info.fragmentapp.fragment;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CalendarView;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.SearchView;

import cis2250.hccis.info.fragmentapp.R;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link FragmentGaudetJohn.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link FragmentGaudetJohn#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FragmentGaudetJohn extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "firstName";
    private static final String ARG_PARAM2 = "lastName";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public FragmentGaudetJohn() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters. test
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment FragmentGaudetJohn.
     */
    // TODO: Rename and change types and number of parameters
    public static FragmentGaudetJohn newInstance(String param1, String param2) {
        FragmentGaudetJohn fragment = new FragmentGaudetJohn();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onStart() {
        super.onStart();
        Button buttonHi = getView().findViewById(R.id.buttonHi);
        final Bundle tempArgs = getArguments();

        if(tempArgs != null){
        buttonHi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d("bjtest", "button clicked. hi");
                Snackbar.make(view, "Hi "+tempArgs.getString(ARG_PARAM1), Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });}else{
            buttonHi.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Log.d("bjtest", "button clicked. hi");
                    Snackbar.make(view, "Hi ", Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();
                }
            });
        }

        //Widget 1: Calendar Widget
        CalendarView simpleCalendarView = getView().findViewById(R.id.calendarView); // get the reference of CalendarView
        // perform setOnDateChangeListener event on CalendarView
        simpleCalendarView.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {
            @Override
            public void onSelectedDayChange(CalendarView view, int year, int month, int dayOfMonth) {
                Snackbar.make(view, "Date Changed: " + year + " " + month + " " + dayOfMonth, Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        //Widget 2: Star Ratings Widget
        /*Add in Oncreate() funtion after setContentView()*/
        RatingBar simpleRatingBar = (RatingBar) getView().findViewById(R.id.ratingBar); // initiate a rating bar
        int numberOfStars = simpleRatingBar.getNumStars(); // get total number of stars of rating bar
        simpleRatingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener(){
            @Override
            public void onRatingChanged(RatingBar ratingBar, float v, boolean b) {
                Snackbar.make(ratingBar, "Value Changed: " + v + " " + b, Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        //Widget 3: Search View Widget
        SearchView simpleSearchView = (SearchView) getView().findViewById(R.id.searchView); // inititate a search view
        CharSequence query = simpleSearchView.getQuery(); // get the query string currently in the text field
        simpleSearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                Snackbar.make(getView(), "Query searched: " + query, Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                Snackbar.make(getView(), "Query searched: " + newText, Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
                return false;
            }
        });
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View temp = inflater.inflate(R.layout.fragment_gaudet_john, container, false);
        return temp;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction("test");
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(String test);
    }
}
