package cis2250.hccis.info.fragmentapp.fragment;

import android.content.Context;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;

import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CalendarView;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.ToggleButton;

import cis2250.hccis.info.fragmentapp.R;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link FragmentPeconiNick.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link FragmentPeconiNick#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FragmentPeconiNick extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "FirstName";
    private static final String ARG_PARAM2 = "LastName";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public FragmentPeconiNick() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment FragmentPeconiNick.
     */
    // TODO: Rename and change types and number of parameters
    public static FragmentPeconiNick newInstance(String param1, String param2) {
        FragmentPeconiNick fragment = new FragmentPeconiNick();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }
    @Override
    public void onStart() {
        super.onStart();
        Button nicksButton = getView().findViewById(R.id.button_nick);
        final RadioButton yellowButton = getView().findViewById(R.id.radioButtonYellow);
        final RadioButton blueButton = getView().findViewById(R.id.radioButtonBlue);
        final CalendarView calender = getView().findViewById(R.id.calendarView);

        calender.setVisibility(View.INVISIBLE);
        nicksButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(calender.getVisibility() == View.VISIBLE){
                    calender.setVisibility(View.INVISIBLE);
                }else{
                    calender.setVisibility(View.VISIBLE);
                    calender.setBackgroundColor(Color.WHITE);
                }


            }

        });
        yellowButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(yellowButton.isChecked()){
                    getView().setBackgroundColor(Color.YELLOW);
                    blueButton.setChecked(false);
                }

            }
        });
        blueButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(blueButton.isChecked()){
                    getView().setBackgroundColor(Color.BLUE);
                    yellowButton.setChecked(false);
                }

            }
        });

        final ToggleButton toggleButton = getView().findViewById(R.id.toggle_nicks);

        toggleButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ImageView image = getView().findViewById(R.id.imageViewHide);
                image.setImageResource(R.mipmap.ic_launcher);

                if(toggleButton.isChecked()){
                    image.setVisibility(View.VISIBLE);

                }else{
                    image.setVisibility(View.INVISIBLE);
                }

            }
        });


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View temp = inflater.inflate(R.layout.fragment_nick, container, false);
        return temp;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            String test = "Hello Nick";
            mListener.onFragmentInteraction(test);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(String test);
    }
}
