package cis2250.hccis.info.fragmentapp.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import cis2250.hccis.info.fragmentapp.R;

/**
 * This fragment contains three widgets as per required.
 */
public class MacDonald_Max_Fragment extends Fragment
{
    public MacDonald_Max_Fragment()
    {
        // Required empty public constructor
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {

        return inflater.inflate(R.layout.fragment_macdonald_max, container, false);
    }
}