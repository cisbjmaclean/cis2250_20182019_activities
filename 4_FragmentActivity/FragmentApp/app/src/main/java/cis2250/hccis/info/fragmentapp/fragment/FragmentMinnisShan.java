package cis2250.hccis.info.fragmentapp.fragment;

import android.content.Context;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.annotation.RequiresApi;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.SeekBar;
import android.widget.Toast;

import cis2250.hccis.info.fragmentapp.R;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link FragmentMinnisShan.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link FragmentMinnisShan#newInstance} factory method to
 * create an instance of this fragment.
 * @author: pminnis
 * @purpose: To control fragment logic
 * @since   2019-01-17
 */
public class FragmentMinnisShan extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "firstName";
    private static final String ARG_PARAM2 = "lastName";
    private ProgressBar progressBar;
    private SeekBar seekbar;
    private RatingBar ratingBar;
    private ProgressBar spinningProgressBar;



    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public FragmentMinnisShan() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters. test
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment FragmentMinnisShan.
     */
    // TODO: Rename and change types and number of parameters
    public static FragmentMinnisShan newInstance(String param1, String param2) {
        FragmentMinnisShan fragment = new FragmentMinnisShan();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void onStart() {

        super.onStart();

        final Bundle tempArgs = getArguments();

        if(tempArgs != null){
            //rating bar id
            RatingBar simpleRatingBar = (RatingBar) getView().findViewById(R.id.ratingBar2); // initiate a rating bar// final float ratingNumber = (float)simpleRatingBar.getRating(); // get rating number from a rating bar
            //rating bar listener
            simpleRatingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
                @Override
                public void onRatingChanged(RatingBar ratingBar, float v, boolean b) {
                    Snackbar.make(ratingBar, "Rating: "+v, Snackbar.LENGTH_LONG).show();
                }
            });

            progressBar = (ProgressBar)getView().findViewById(R.id.progressBar2);
            spinningProgressBar= (ProgressBar)getView().findViewById(R.id.progressBar3);
            seekbar = (SeekBar)getView().findViewById(R.id.seekBar);
            // linking seekbar to progress bars
            seekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

                @Override
                public void onProgressChanged(SeekBar seekBar, final int progress, boolean b) {
                    Log.d("ShannyDebug", "onProgressChanged: "+progress);
                    progressBar.setProgress(progress);
                    //progress bar will disappear if seek bar reaches 100
                    if(progress >= 100){
                        spinningProgressBar.setVisibility(View.INVISIBLE);
                    }else{
                        spinningProgressBar.setVisibility(View.VISIBLE);
                    }
                }
                @Override
                public void onStartTrackingTouch(SeekBar seekBar) {
                }
                @Override
                public void onStopTrackingTouch(SeekBar seekBar) {
                }
            });
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View temp = inflater.inflate(R.layout.fragment_minnis_shan, container, false);
        return temp;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
