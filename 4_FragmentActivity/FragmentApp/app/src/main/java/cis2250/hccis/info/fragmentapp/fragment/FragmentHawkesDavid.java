package cis2250.hccis.info.fragmentapp;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link BlankFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link BlankFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FragmentHawkesDavid extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public FragmentHawkesDavid() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment BlankFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static FragmentHawkesDavid newInstance(String param1, String param2) {
        FragmentHawkesDavid fragment = new FragmentHawkesDavid();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public void onStart() {

        super.onStart();

        //Get Views of the widgets
        Button resetButton = getView().findViewById(R.id.buttonResetDavid);
        RatingBar ratingBar = getView().findViewById(R.id.ratingBarDavid);
        final TextView textView = getView().findViewById(R.id.textViewSearchDavid);
        final SearchView searchView = getView().findViewById(R.id.searchViewDavid);
        final ProgressBar progressBar = getView().findViewById(R.id.progressBarDavid);

        final Bundle tempArgs = getArguments();

        if(tempArgs != null){

            /**
             * This method will update a progress bar based on the input from the user on a rating bar.
             *
             * @author David Hawkes
             * @since 2019/01/22
             */
            ratingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {

                @Override
                public void onRatingChanged(RatingBar ratingBar, float v, boolean b) {

                    //Get the rating
                    double rating = ratingBar.getRating();

                    //Log the rating
                    Log.d("BJTEST", "rating = " + rating);
                    //Pass the rating to the main activity.
                    mListener.onFragmentInteraction("The rating is " + rating);

                    //Update the progress bar based on the rating.
                    if (rating == 0.5) {
                        progressBar.setProgress(10);

                    } else if (rating == 1.0) {
                        progressBar.setProgress(20);

                    } else if (rating == 1.5) {
                        progressBar.setProgress(30);

                    } else if (rating == 2.0) {
                        progressBar.setProgress(40);

                    } else if (rating == 2.5) {
                        progressBar.setProgress(50);

                    } else if (rating == 3.0) {
                        progressBar.setProgress(60);

                    } else if (rating == 3.5) {
                        progressBar.setProgress(70);

                    } else if (rating == 4.0) {
                        progressBar.setProgress(80);

                    } else if (rating == 4.5) {
                        progressBar.setProgress(90);

                    } else if (rating == 5.0) {
                        progressBar.setProgress(100);

                    } else {
                        progressBar.setProgress(0);
                    }
                }
            });

            /**
             * This method will change the text view text based on a search view.
             *
             * @author David Hawkes
             * @since 2019/01/22
             */
            searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {

                //Text view text will be changed when a query is submitted.
                @Override
                public boolean onQueryTextSubmit(String s) {

                    //Get the entered text
                    String enteredText = searchView.getQuery().toString();

                    //Log that a query has been submitted.
                    Log.d("bjtest", "search view text has been entered.");
                    //Pass the entered text to the main activity.
                    mListener.onFragmentInteraction("The entered text is " + enteredText);
                    //Set the text view text to the entered text.
                    textView.setText(enteredText);

                    return false;
                }

                //Text view text will change whenever the query is changed.
                @Override
                public boolean onQueryTextChange(String s) {

                    //Set the text view text to the new query.
                    textView.setText(searchView.getQuery().toString());

                    return false;
                }
            });

            /**
             * This method will reset the search view text and text view text whenever the button is clicked.
             *
             * @author David Hawkes
             * @since 2019/01/22
             */
            resetButton.setOnClickListener(new View.OnClickListener(){

                @Override
                public void onClick(View view) {
                    textView.setText("");
                    searchView.setQuery("", false);
                    searchView.clearFocus();
                }
            });
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View temp = inflater.inflate(R.layout.fragment_hawkes_david, container, false);
        return temp;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(String text) {
        if (mListener != null) {
            mListener.onFragmentInteraction(text);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(String text);
    }
}
