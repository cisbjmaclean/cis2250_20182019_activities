package cis2250.hccis.info.fragmentapp.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CalendarView;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import cis2250.hccis.info.fragmentapp.R;

/**
 * This is a Fragment. There are three widgets such as Calendar, Rating bar, and Image view
 *
 * Author : Taeju Hwang
 * Date : 2019-01-21
 */
public class FragmentHwangTaeju extends Fragment {


    //Private attributes
    private CalendarView aCalendarView;
    private TextView text;
    private RatingBar ratingBar;
    private ImageView image;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.activity_fragment1, container, false);
    }

    @Override
    public void onStart() {
        super.onStart();
        //Get the calendar view id and display the date on Text view.
        aCalendarView = getView().findViewById(R.id.calendarView);
        text = getView().findViewById(R.id.textView1);
        aCalendarView.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {
            @Override
            public void onSelectedDayChange(CalendarView view, int year, int month, int dayOfMonth) {
                String date = year + "/" + (month+1) + "/" + dayOfMonth;
                Log.d("Calendar", "onSelectedDayChange: yyyy/mm/dd:" +  date);
                text.setText(date);
            }
        });
        //Get the rating bar id and display it on Toast
        ratingBar = getView().findViewById(R.id.ratingBar);
        ratingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                Toast.makeText(getActivity(), String.valueOf(rating), Toast.LENGTH_LONG).show();
            }
        });
        //set a image on image view
        image = getView().findViewById(R.id.imageView);
        image.setImageResource(R.drawable.dog);

    }
}
