
package cis2250.hccis.info.fragmentapp.fragment;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.SeekBar;

import cis2250.hccis.info.fragmentapp.R;


/**
 * Creating a fragment that works with three different widgets, creates a snackbar when the widget is
 * interacted with
 * Darcy Watts
 * 2019/01/21
 */
public class FragmentWattsDarcy extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "firstName";
    private static final String ARG_PARAM2 = "lastName";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;


    private OnFragmentInteractionListener mListener;

    public FragmentWattsDarcy() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters. test
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment FragmentMacLeanBJ.
     */
    // TODO: Rename and change types and number of parameters
    public static FragmentWattsDarcy newInstance(String param1, String param2) {
        FragmentWattsDarcy fragment = new FragmentWattsDarcy();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onStart() {
        super.onStart();
        //Setting up widgets
        ProgressBar progress = getView().findViewById(R.id.progressBar4);
        SeekBar seek = getView().findViewById(R.id.seekBar);
        RatingBar rating = getView().findViewById(R.id.ratingBar2);
        final Bundle tempArgs = getArguments();

        //If there are arguments passed to the fragment this block of code will run if not the other block will
        if(tempArgs != null){
            //If the progress bar is clicked a snackbar is made
            progress.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Log.d("DWTEST",tempArgs.getString(ARG_PARAM1)+" You clicked the progress bar");
                    Snackbar.make(view, "Hi "+tempArgs.getString(ARG_PARAM1)+" You have clicked the progress bar", Snackbar.LENGTH_LONG)
                            .setAction("", null).show();
                }
            });

            //If the seek bar is clicked a snackbar will let the user know how full the seek bar is
            seek.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                @Override
                public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                    View view = getActivity().findViewById(android.R.id.content);
                    Log.d("DWTEST", tempArgs.getString(ARG_PARAM1)+" The progress bar is: "+i+"% full");
                    Snackbar.make(view, "Hi "+tempArgs.getString(ARG_PARAM1)+" the progress bar is: "+i+"% full", Snackbar.LENGTH_LONG)
                            .setAction("", null).show();

                }

                @Override
                public void onStartTrackingTouch(SeekBar seekBar) {

                }

                @Override
                public void onStopTrackingTouch(SeekBar seekBar) {

                }
            });

            //If the rating bar is clicked a snackbar will let the user know how many stars were clicked
            rating.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
                @Override
                public void onRatingChanged(RatingBar ratingBar, float v, boolean b) {
                    View view = getActivity().findViewById(android.R.id.content);
                    Log.d("DWTEST", tempArgs.getString(ARG_PARAM1)+" have rated this= "+v+" stars");

                    Snackbar.make(view, "Hi "+tempArgs.getString(ARG_PARAM1)+" You have rated this: "+v+"/5", Snackbar.LENGTH_LONG)
                            .setAction("", null).show();

                }
            });
    }else{

            //If the progress bar is clicked a snackbar is made
            progress.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Log.d("DWTEST", "You have clicked the progress bar");
                    Snackbar.make(view, "Hi, You have clicked the progress bar", Snackbar.LENGTH_LONG)
                            .setAction("", null).show();
                }
            });

            //If the rating bar is clicked a snackbar will let the user know how many stars were clicked
            rating.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
                @Override
                public void onRatingChanged(RatingBar ratingBar, float v, boolean b) {
                    View view = getActivity().findViewById(android.R.id.content);
                    Log.d("DWTEST","The rating bar is at: "+v);
                    Snackbar.make(view, "Hi, You have rated this a: "+v+"/5", Snackbar.LENGTH_LONG)
                            .setAction("", null).show();
                }
            });

            //If the seek bar is clicked a snackbar will let the user know how full the seek bar is
            seek.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                @Override
                public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                    View view = getActivity().findViewById(android.R.id.content);
                    Log.d("DWTEST", "the seekbar is "+i+"% full");
                    Snackbar.make(view, "Hi, the progress bar is: "+i+"% full", Snackbar.LENGTH_LONG)
                            .setAction("", null).show();
                }

                @Override
                public void onStartTrackingTouch(SeekBar seekBar) {

                }

                @Override
                public void onStopTrackingTouch(SeekBar seekBar) {

                }
            });
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View temp = inflater.inflate(R.layout.fragment_watts_darcy, container, false);
        return temp;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(123);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(int date);
    }
}
