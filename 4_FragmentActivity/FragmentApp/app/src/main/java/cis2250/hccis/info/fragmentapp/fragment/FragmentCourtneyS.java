
package cis2250.hccis.info.fragmentapp.fragment;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RatingBar;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.Toast;

import cis2250.hccis.info.fragmentapp.R;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link FragmentMacLeanBJ.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link FragmentMacLeanBJ#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FragmentCourtneyS extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "firstName";
    private static final String ARG_PARAM2 = "lastName";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public FragmentCourtneyS() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters. test
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment FragmentMacLeanBJ.
     */
    // TODO: Rename and change types and number of parameters
    public static FragmentCourtneyS newInstance(String param1, String param2) {
        FragmentCourtneyS fragment = new FragmentCourtneyS();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onStart() {
        super.onStart();

        //imageview fragment
        final ImageView imageView3 = (ImageView) getView().findViewById(R.id.imageView3);//get the id of first image view

        imageView3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d("bjtest", "image clicked");
                Snackbar.make(imageView3, "It's a cute picture!", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
        //end image view stuff

        //The button BJ used in the demo modified to be one of the scroll view button actions
        Button buttonHello = getView().findViewById(R.id.buttonHello);
        final Bundle tempArgs = getArguments();

        if (tempArgs != null) {
            buttonHello.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Log.d("bjtest", "button clicked. hi");
                    Snackbar.make(view, "You did it " + tempArgs.getString(ARG_PARAM1), Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();
                }
            });
        } else {
            buttonHello.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Log.d("bjtest", "button clicked. hi");
                    Snackbar.make(view, "Hi ", Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();
                }
            });
        }

        /*modified from https://abhiandroid.com/ui/ratingbar#Methods_Used_in_Rating_Bar*/
        /*the star rating bar*/
        RatingBar simpleRatingBar = (RatingBar) getView().findViewById(R.id.ratingBar); // start a rating bar
        int numberOfStars = simpleRatingBar.getNumStars(); // get total number of stars of rating bar

        simpleRatingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float v, boolean b) {
                Log.d("rating", "Rating Selected");
                Snackbar.make(ratingBar, "You have given this picture a " + v + " star rating!", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        /*modified from https://abhiandroid.com/ui/ratingbar#Methods_Used_in_Rating_Bar*/
        /*slider bar, demos snackbar telling you how far along the bar you slid it*/
        SeekBar simpleSeekBar = (SeekBar) getView().findViewById(R.id.simpleSeekBar); // initiate the Seek bar

        int seekBarValue = simpleSeekBar.getProgress(); // get progress value from the Seek bar

        simpleSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                Log.d("rating", "Bar Selected");
                Snackbar.make(seekBar, "You have moved it to " + i + "%!", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }

        });


    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View temp = inflater.inflate(R.layout.fragment_courtney_s, container, false);
        return temp;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction("test");
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(String test);
    }
}
