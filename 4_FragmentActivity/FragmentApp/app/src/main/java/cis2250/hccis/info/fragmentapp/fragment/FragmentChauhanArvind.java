package cis2250.hccis.info.fragmentapp;

import android.content.Context;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CalendarView;
import android.widget.RatingBar;
import android.widget.SearchView;
import android.widget.Switch;

import cis2250.hccis.info.fragmentapp.fragment.FragmentTownsendJ;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link FragmentChauhanArvind.OnFragmentInteractionListener} interface
 * to handle interaction events.
 */
public class FragmentChauhanArvind extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "firstName";
    private static final String ARG_PARAM2 = "lastName";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public FragmentChauhanArvind() {
        // Required empty public constructor
    }
    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters. test
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment FragmentTownsendJ.
     */
    // TODO: Rename and change types and number of parameters
    public static FragmentChauhanArvind newInstance(String param1, String param2) {
        FragmentChauhanArvind fragment = new FragmentChauhanArvind();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }
    @Override

    public void onStart(){
        super.onStart();

        RatingBar ratingBar = getView().findViewById(R.id.arvindRatingBar);
        CalendarView calendarView = getView().findViewById(R.id.arvindCalendarView);
        final Switch switchOnOff = getView().findViewById(R.id.arvindSwitch_OnOff);

        final Bundle tempArgs = getArguments();

        if(tempArgs != null){
            /** when a date is clicked, a message will show up displaying the date */
            calendarView.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {
            @Override
            public void onSelectedDayChange(@NonNull CalendarView calendarView, int i, int i1, int i2) {
              Log.d("actest", "calendar clicked.");
              String date = (i1+ 1)+ "/" + i2 + "/" + i;
              Snackbar.make(calendarView, "Selected date is:  " + date    , Snackbar.LENGTH_LONG)
              .setAction("Action", null).show();
                 }
              }

            );
            /** when the switch is toggled off/on it will change the background color */

            switchOnOff.setOnClickListener(new View.OnClickListener(){

                @Override
                public void onClick(View view) {
                    if (switchOnOff.isChecked()){
                        switchOnOff.setText("Change Back");
                        getView().setBackgroundColor(Color.CYAN);
                    }else if (!switchOnOff.isChecked()){
                        getView().setBackgroundColor(Color.WHITE);
                        switchOnOff.setText("Change Color");
                    }
                }
            });

            /** when the rating bar is clicked on, a message will appear displaying the users rating */
            ratingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
                @Override

                public void onRatingChanged(RatingBar ratingBar, float v, boolean b) {


                    Snackbar.make(ratingBar, "Selected rating is: " + v, Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();
                }
            });
        }else{
            calendarView.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {
                @Override
                public void onSelectedDayChange(@NonNull CalendarView calendarView, int i, int i1, int i2) {
                    Log.d("actest", "calendar clicked.");
                    Snackbar.make(calendarView, "Hi ", Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();
                }
            });
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_chauhan_arvind, container, false);
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
