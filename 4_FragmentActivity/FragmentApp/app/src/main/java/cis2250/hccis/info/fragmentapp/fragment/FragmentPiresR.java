package cis2250.hccis.info.fragmentapp.fragment;

import android.app.DatePickerDialog;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AnalogClock;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.DigitalClock;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Calendar;

import cis2250.hccis.info.fragmentapp.R;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link FragmentPiresR.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link FragmentPiresR#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FragmentPiresR extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;


    public FragmentPiresR() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment FragmentPiresR.
     */
    // TODO: Rename and change types and number of parameters
    public static FragmentPiresR newInstance(String param1, String param2) {
        FragmentPiresR fragment = new FragmentPiresR();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onStart() {
        super.onStart();

        final Bundle tempArgs = getArguments();
        Button buttonRodrigo = getView().findViewById(R.id.buttonRodrigo);

        final EditText date;


        //Button BJ (Refactored)
        if (tempArgs != null) {
            buttonRodrigo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Log.d("bjtest", "button clicked. hi");
                    Snackbar.make(view, "Hi "+tempArgs.getString(ARG_PARAM1), Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();
                }
            });}else{
            buttonRodrigo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Log.d("bjtest", "button clicked. hi");
                    Snackbar.make(view, "Hi ", Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();
                }
            });
        }

        //Analog Clock
        AnalogClock simpleAnalogClock = getView().findViewById(R.id.simpleAnalogClock);
        // perform click event on analog clock
        simpleAnalogClock.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getContext(), "Analog Clock", Toast.LENGTH_SHORT).show(); // display a toast for analog clock
            }
        });

        //Digital Clock
        DigitalClock simpleDigitalClock = getView().findViewById(R.id.simpleDigitalClock);

        simpleDigitalClock.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getContext(), "Digital Clock", Toast.LENGTH_SHORT).show(); //display a toast for digital clock
            }
        });



        //Date Picker
        date = getView().findViewById(R.id.date);
        // perform click event on edit text
        date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // calender class's instance and get current date , month and year from calender
                final Calendar c = Calendar.getInstance();
                int mYear = c.get(Calendar.YEAR); // current year
                int mMonth = c.get(Calendar.MONTH); // current month
                int mDay = c.get(Calendar.DAY_OF_MONTH); // current day
                // date picker dialog
                DatePickerDialog datePickerDialog;
                datePickerDialog = new DatePickerDialog(getContext(),
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {
                                // set day of month , month and year value in the edit text
                                date.setText(dayOfMonth + "/"
                                        + (monthOfYear + 1) + "/" + year);
                                Snackbar.make(view, "The date is " + date.getText().toString(), Snackbar.LENGTH_LONG)
                                        .setAction("Action", null).show();

                            }
                        }, mYear, mMonth, mDay);
                datePickerDialog.show();
            }
        });


        //Timer
        Button buttonTimer;
        final TextView textViewTimer;
        buttonTimer = getView().findViewById(R.id.button);
        textViewTimer= getView().findViewById(R.id.textView);
        buttonTimer.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v) {

                    new CountDownTimer(10000, 1000){
                        int counter;
                        public void onTick(long millisUntilFinished){
                            textViewTimer.setText(String.valueOf(counter));
                            counter++;
                        }
                        public  void onFinish(){
                            textViewTimer.setText("DONE!");
                        }
                    }.start();
                }
            });


    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }



    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_fragment_pires_r, container, false);
    }


    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction("test");
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(String test);
    }
}
