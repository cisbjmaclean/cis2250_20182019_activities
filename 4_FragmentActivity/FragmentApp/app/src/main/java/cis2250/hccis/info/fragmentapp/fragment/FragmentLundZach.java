package cis2250.hccis.info.fragmentapp.fragment;


import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.CalendarView;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;

import cis2250.hccis.info.fragmentapp.R;


/**
 * A simple {@link Fragment} subclass.
 */


    /**
     * A simple {@link Fragment} subclass.
     * Activities that contain this fragment must implement the
     * {@link cis2250.hccis.info.fragmentapp.FragmentMacLeanBJ.OnFragmentInteractionListener} interface
     * to handle interaction events.
     * Use the {@link cis2250.hccis.info.fragmentapp.FragmentMacLeanBJ#newInstance} factory method to
     * create an instance of this fragment.
     */
    public class FragmentLundZach extends Fragment {
        // TODO: Rename parameter arguments, choose names that match
        // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
        private static final String ARG_PARAM1 = "firstName";
        private static final String ARG_PARAM2 = "lastName";

        // TODO: Rename and change types of parameters
        private String mParam1;
        private String mParam2;

        private cis2250.hccis.info.fragmentapp.FragmentMacLeanBJ.OnFragmentInteractionListener mListener;

        public FragmentLundZach() {
            // Required empty public constructor
        }

        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters. test
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment FragmentMacLeanBJ.
         */
        // TODO: Rename and change types and number of parameters
        public static FragmentLundZach newInstance(String param1, String param2) {
            FragmentLundZach fragment = new FragmentLundZach();
            Bundle args = new Bundle();
            args.putString(ARG_PARAM1, param1);
            args.putString(ARG_PARAM2, param2);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public void onStart() {
            super.onStart();
            final Bundle tempArgs = getArguments();
            final Button buttonStar = getView().findViewById(R.id.buttonShow);
            final ImageView star = getView().findViewById(R.id.imageViewStar);
            final CalendarView calendarView = getView().findViewById(R.id.calendarView);
            final SeekBar seekBar = getView().findViewById(R.id.seekBar);
            final TextView textViewSeek = getView().findViewById(R.id.textViewSeekBar);
            final TextView textViewCalender = getView().findViewById(R.id.textViewCalender);


            if(tempArgs != null) {

                seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                    int progressValue;

                    @Override
                    public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                            progressValue = i;
                    }

                    @Override
                    public void onStartTrackingTouch(SeekBar seekBar) {

                    }

                    @Override
                    public void onStopTrackingTouch(SeekBar seekBar) {
                        textViewSeek.setText(progressValue + "/" + seekBar.getMax());
                    }
                });





                buttonStar.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        if (star.getVisibility() == View.VISIBLE){

                        star.setVisibility(View.INVISIBLE);

                    }else{
                            star.setVisibility(View.VISIBLE);
                        }
                }});

                calendarView.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {
                    @Override
                    public void onSelectedDayChange(@NonNull CalendarView calendarView, int i, int i1, int i2) {
                        String date = (i1 + 1) + "/" + i2 + "/" + i;
                        textViewCalender.setText(date);
                    }
                });



            }






            }


        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            if (getArguments() != null) {
                mParam1 = getArguments().getString(ARG_PARAM1);
                mParam2 = getArguments().getString(ARG_PARAM2);
            }

        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {

            // Inflate the layout for this fragment
            View temp = inflater.inflate(R.layout.fragment_fragment_lund_zach, container, false);




            return temp;
        }

        // TODO: Rename method, update argument and hook method into UI event
        public void onButtonPressed(Uri uri) {
            if (mListener != null) {
                mListener.onFragmentInteraction("test");
            }
        }

        @Override
        public void onAttach(Context context) {
            super.onAttach(context);
            if (context instanceof cis2250.hccis.info.fragmentapp.FragmentMacLeanBJ.OnFragmentInteractionListener) {
                mListener = (cis2250.hccis.info.fragmentapp.FragmentMacLeanBJ.OnFragmentInteractionListener) context;
            } else {
                throw new RuntimeException(context.toString()
                        + " must implement OnFragmentInteractionListener");
            }
        }

        @Override
        public void onDetach() {
            super.onDetach();
            mListener = null;
        }

        /**
         * This interface must be implemented by activities that contain this
         * fragment to allow an interaction in this fragment to be communicated
         * to the activity and potentially other fragments contained in that
         * activity.
         * <p>
         * See the Android Training lesson <a href=
         * "http://developer.android.com/training/basics/fragments/communicating.html"
         * >Communicating with Other Fragments</a> for more information.
         */
        public interface OnFragmentInteractionListener {
            // TODO: Update argument type and name
            void onFragmentInteraction(String test);
        }
    }
