package cis2250.hccis.info.fragmentapp.fragment;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.DividerItemDecoration;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CalendarView;
import android.widget.ImageView;
import android.widget.RatingBar;

import cis2250.hccis.info.fragmentapp.R;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link FragmentTownsendJ.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link FragmentTownsendJ#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FragmentTownsendJ extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "firstName";
    private static final String ARG_PARAM2 = "lastName";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public FragmentTownsendJ() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters. test
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment FragmentTownsendJ.
     */
    // TODO: Rename and change types and number of parameters
    public static FragmentTownsendJ newInstance(String param1, String param2) {
        FragmentTownsendJ fragment = new FragmentTownsendJ();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onStart() {
        super.onStart();

        CalendarView calendarView = getView().findViewById(R.id.calendarView);
        ImageView simpleImageView=getView().findViewById(R.id.simpleImageView);
        RatingBar ratingBar = getView().findViewById(R.id.ratingBar);


        final Bundle tempArgs = getArguments();

        if(tempArgs != null){
            /** when a date is clicked, a message will show up displaying the date */
        calendarView.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {
            @Override
            public void onSelectedDayChange(@NonNull CalendarView calendarView, int i, int i1, int i2) {
                Log.d("jttest", "calendar clicked.");
               String date = (i1+ 1)+ "/" + i2 + "/" + i;
                Snackbar.make(calendarView, "Your date is:  " + date    , Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        }

        );
        /** when the image is clicked, a message will appear */
            simpleImageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Snackbar.make(view, "Do you like cookies?", Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();
                }
            });
            /** when the rating bar is clicked on, a message will appear displaying the users rating */
            ratingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
                @Override

                public void onRatingChanged(RatingBar ratingBar, float v, boolean b) {


                    Snackbar.make(ratingBar, "Your rating is: " + v, Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();
                }
            });
        }else{
            calendarView.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {
                @Override
                public void onSelectedDayChange(@NonNull CalendarView calendarView, int i, int i1, int i2) {
                    Log.d("jttest", "calendar clicked.");
                    Snackbar.make(calendarView, "Hi ", Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();
                }
            });
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View temp = inflater.inflate(R.layout.fragment_townsend_j, container, false);
        return temp;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
