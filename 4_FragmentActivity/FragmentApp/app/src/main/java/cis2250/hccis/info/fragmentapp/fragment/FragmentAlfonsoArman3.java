/*
 * Student: Arman Alfonso
 * Course: CIS2250
 * Assignment: #1 Fragment
 */
package cis2250.hccis.info.fragmentapp.fragment;

import android.content.Context;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.SeekBar;
import android.widget.Switch;
import android.widget.TextClock;
import android.widget.TextView;
import android.widget.Toast;

import cis2250.hccis.info.fragmentapp.FragmentMacLeanBJ;
import cis2250.hccis.info.fragmentapp.MainActivity;
import cis2250.hccis.info.fragmentapp.R;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link FragmentAlfonsoArman3#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FragmentAlfonsoArman3 extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public FragmentAlfonsoArman3() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment FragmentAlfonsoArman3.
     */
    // TODO: Rename and change types and number of parameters
    public static FragmentAlfonsoArman3 newInstance(String param1, String param2) {
        FragmentAlfonsoArman3 fragment = new FragmentAlfonsoArman3();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_alfonso_arman3, container, false);
        String[] timeZones = {"Select Time Zone",
                "Canada/Atlantic",
                "Canada/Eastern",
                "Canada/Central",
                "Canada/Mountain",
                "Canada/Pacific"};

        ListView listView;
        listView = (ListView) view.findViewById(R.id.listViewZone);

        ArrayAdapter<String> listViewAdapter = new ArrayAdapter<String>(
                getActivity(),
                android.R.layout.simple_list_item_1,
                timeZones
        );

        //This will show a dropdown for timezones

        listView.setAdapter(listViewAdapter);

        return view;

    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onStart() {
        super.onStart();

        //widget 1 - the switch will change the background color to either Red or Blue

        final Switch switchColor = (Switch) getView().findViewById(R.id.switchBackground);
        final TextClock tc = (TextClock) getView().findViewById(R.id.textClock1);

        switchColor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(switchColor.isChecked()){
                    getView().setBackgroundColor(Color.RED);
                    switchColor.setText("Change to BLUE");
                } else {
                    getView().setBackgroundColor(Color.BLUE);
                    switchColor.setText("Change to Red");
                }
            }
        });

        //widget 2 ListView and 3 TextClock
        // ListView tzone will allow selection of time zones
        // TextClock textClock1 will be changed based on the tzone selection

        final ListView tzone = (ListView) getView().findViewById(R.id.listViewZone);
        final TextView labelForClock = getView().findViewById(R.id.labelForClock);

        tzone.setClickable(true);
        tzone.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {

                Object x = tzone.getItemAtPosition(position);

                //save selection
                String selected = (String) x;
                if (selected.equals("Select Time Zone")){
                    //do nothing
                } else {
                    //change timezone based on selection
                    labelForClock.setText("Current Time: " + selected);
                    tc.setTimeZone(selected);
                }
            }
        });
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
