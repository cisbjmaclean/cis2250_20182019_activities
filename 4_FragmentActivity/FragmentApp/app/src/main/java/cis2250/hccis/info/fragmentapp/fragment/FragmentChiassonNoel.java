package cis2250.hccis.info.fragmentapp;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.SeekBar;
import android.widget.Toast;

import java.util.Objects;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link FragmentChiassonNoel.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link FragmentChiassonNoel#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FragmentChiassonNoel extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "firstName";
    private static final String ARG_PARAM2 = "lastName";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public FragmentChiassonNoel() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters. test
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment FragmentMacLeanBJ.
     */
    // TODO: Rename and change types and number of parameters
    public static FragmentChiassonNoel newInstance(String param1, String param2) {
        FragmentChiassonNoel fragment = new FragmentChiassonNoel();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onStart() {
        super.onStart();
        Button buttonHi = getView().findViewById(R.id.buttonHi);

        //Unique Widgets
        final RatingBar ratingBar = getView().findViewById(R.id.ratingBar);
        final SeekBar seekBar = getView().findViewById(R.id.seekBar);
        final ProgressBar progressBar = getView().findViewById(R.id.progressBar);



        final Bundle tempArgs = getArguments();

        if(tempArgs != null){
        buttonHi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d("bjtest", "button clicked. hi");
                Snackbar.make(view, "Hi "+tempArgs.getString(ARG_PARAM1), Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        /*
        ratingBar.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                RatingBar ratBar = getView().findViewById(R.id.ratingBar);
                float rating = ratBar.getRating();
                seekBar.setProgress((int)(rating * 100));
                progressBar.setProgress((int)(rating * 100));
            }
        });*/

        ratingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating,
                                        boolean fromUser) {
                if(fromUser){
                    seekBar.setProgress((int)(rating * 20));
                    progressBar.setProgress((int)(rating * 20));
                }
            }
        });

        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onStartTrackingTouch(SeekBar seek){
                Log.d("BJTEST", "SEEKBAR IS TRACKING START");
            }

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser){
                if(fromUser){
                    progressBar.setProgress(progress);
                    ratingBar.setRating((float)(0.5 * (progress / 10)));
                }
            }

            @Override
            public void onStopTrackingTouch(SeekBar seek){
                Log.d("BJTEST", "SEEKBAR IS TRACKING STOP");
            }
        });

        }
        else{
            buttonHi.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Log.d("bjtest", "button clicked. hi");
                    Snackbar.make(view, "Hi ", Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();
                }
            });
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View temp = inflater.inflate(R.layout.fragment_chiasson_noel, container, false);
        return temp;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
