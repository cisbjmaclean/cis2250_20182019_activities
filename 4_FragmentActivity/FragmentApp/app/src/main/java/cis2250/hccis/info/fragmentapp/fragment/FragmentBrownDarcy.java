package cis2250.hccis.info.fragmentapp;

import android.content.Context;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.Switch;

import java.util.Timer;
import java.util.TimerTask;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link FragmentBrownDarcy.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link FragmentBrownDarcy#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FragmentBrownDarcy extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;
    public int progress = 0;

    public FragmentBrownDarcy() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment FragmentBrownDarcy.
     */
    // TODO: Rename and change types and number of parameters
    public static FragmentBrownDarcy newInstance(String param1, String param2) {
        FragmentBrownDarcy fragment = new FragmentBrownDarcy();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_brown_darcy, container, false);
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }


    @Override
    public void onStart() {
        super.onStart();

        final Switch switchOnOff = (Switch) getView().findViewById(R.id.switch_OnOff);
        final SeekBar seekBar = (SeekBar) getView().findViewById(R.id.seekBar_Color);
        final ProgressBar progressBar = (ProgressBar) getView().findViewById(R.id.progressBar6);

        new Timer().scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                progressBar.setProgress(progress++);
                if(progressBar.getProgress() == 100){
                    progressBar.setProgress(0);
                }
            }
        }, 0, 1000);



        switchOnOff.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(switchOnOff.isChecked()){

                    switchOnOff.setText("Turned On");
                    getView().setBackgroundColor(Color.YELLOW);
                    seekBar.setVisibility(View.VISIBLE);

                    seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                        @Override
                        public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                            if(i == 0){
                                getView().setBackgroundColor(Color.YELLOW);
                            } else if(i == 1){
                                getView().setBackgroundColor(Color.BLUE);
                            } else if(i == 2){
                                getView().setBackgroundColor(Color.GREEN);
                            } else if(i == 3){
                                getView().setBackgroundColor(Color.RED);
                            } else if(i == 4){
                                getView().setBackgroundColor(Color.GRAY);
                            } else if(i == 5){
                                getView().setBackgroundColor(Color.CYAN);
                            } else if(i == 6){
                                getView().setBackgroundColor(Color.MAGENTA);
                            } else if(i == 7) {
                                getView().setBackgroundColor(Color.DKGRAY);
                            }
                        }

                        @Override
                        public void onStartTrackingTouch(SeekBar seekBar) {

                        }

                        @Override
                        public void onStopTrackingTouch(SeekBar seekBar) {

                        }
                    });
                } else if (!switchOnOff.isChecked()){
                    seekBar.setVisibility(View.GONE);
                    seekBar.setProgress(0);
                    getView().setBackgroundColor(Color.WHITE);
                    switchOnOff.setText("Turned Off");
                }
            }
        });
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
