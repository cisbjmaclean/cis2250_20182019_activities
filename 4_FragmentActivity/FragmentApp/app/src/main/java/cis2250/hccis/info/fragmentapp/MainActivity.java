package cis2250.hccis.info.fragmentapp;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.DialogInterface;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import cis2250.hccis.info.fragmentapp.fragment.FragmentAkmelMeric;
import cis2250.hccis.info.fragmentapp.fragment.FragmentAlfonsoArman3;
import cis2250.hccis.info.fragmentapp.fragment.FragmentBlanchardAshley;
import cis2250.hccis.info.fragmentapp.fragment.FragmentBryanMatt;
import cis2250.hccis.info.fragmentapp.fragment.FragmentChandlerBonnie;
import cis2250.hccis.info.fragmentapp.fragment.FragmentCourtneyS;
import cis2250.hccis.info.fragmentapp.fragment.FragmentCurranJason;
import cis2250.hccis.info.fragmentapp.fragment.FragmentGaudetJohn;
import cis2250.hccis.info.fragmentapp.fragment.FragmentHwangTaeju;
import cis2250.hccis.info.fragmentapp.fragment.FragmentJohnstonAshley;
import cis2250.hccis.info.fragmentapp.fragment.FragmentLundZach;
import cis2250.hccis.info.fragmentapp.fragment.FragmentMacNeillIsaiah;
import cis2250.hccis.info.fragmentapp.fragment.FragmentMartinSteve;
import cis2250.hccis.info.fragmentapp.fragment.FragmentMinnisShan;
import cis2250.hccis.info.fragmentapp.fragment.FragmentMorrisonEvan;
import cis2250.hccis.info.fragmentapp.fragment.FragmentOrakMusluSinem;
import cis2250.hccis.info.fragmentapp.fragment.FragmentPeconiNick;
import cis2250.hccis.info.fragmentapp.fragment.FragmentPierreAshleigh;
import cis2250.hccis.info.fragmentapp.fragment.FragmentPiresR;
import cis2250.hccis.info.fragmentapp.fragment.FragmentTownsendJ;
import cis2250.hccis.info.fragmentapp.fragment.FragmentWattsDarcy;
import cis2250.hccis.info.fragmentapp.fragment.MacDonald_Max_Fragment;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener
        , FragmentMacNeillIsaiah.OnFragmentInteractionListener
        , cis2250.hccis.info.fragmentapp.FragmentChauhanArvind.OnFragmentInteractionListener
        , FragmentBlanchardAshley.OnFragmentInteractionListener
        , FragmentAlfonsoArman3.OnFragmentInteractionListener
        , FragmentCurranJason.OnFragmentInteractionListener
        , FragmentAkmelMeric.OnFragmentInteractionListener
        , FragmentChandlerBonnie.OnFragmentInteractionListener
        , cis2250.hccis.info.fragmentapp.FragmentAndersonBrandon.OnFragmentInteractionListener
        , cis2250.hccis.info.fragmentapp.FragmentBrownDarcy.OnFragmentInteractionListener
        , FragmentTownsendJ.OnFragmentInteractionListener
        , FragmentGaudetJohn.OnFragmentInteractionListener
        , FragmentPeconiNick.OnFragmentInteractionListener
        , cis2250.hccis.info.fragmentapp.FragmentChiassonNoel.OnFragmentInteractionListener
        , FragmentCourtneyS.OnFragmentInteractionListener
        , FragmentBryanMatt.OnFragmentInteractionListener
        , FragmentMacLeanBJ.OnFragmentInteractionListener
        , cis2250.hccis.info.fragmentapp.FragmentCody.OnFragmentInteractionListener
        , cis2250.hccis.info.fragmentapp.FragmentHawkesDavid.OnFragmentInteractionListener
        , FragmentJohnstonAshley.OnFragmentInteractionListener
        , cis2250.hccis.info.fragmentapp.FragmentLewisRyan.OnFragmentInteractionListener
        , FragmentLundZach.OnFragmentInteractionListener
        , FragmentMartinSteve.OnFragmentInteractionListener
        , FragmentMinnisShan.OnFragmentInteractionListener
        , FragmentMorrisonEvan.OnFragmentInteractionListener
        , FragmentOrakMusluSinem.OnFragmentInteractionListener
        , FragmentPierreAshleigh.OnFragmentInteractionListener
        , FragmentPiresR.OnFragmentInteractionListener
        , FragmentWattsDarcy.OnFragmentInteractionListener {


    FragmentMacLeanBJ fragment1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        //******************************************************************************************
        // Set the initial fragment on the main activity layout
        //******************************************************************************************

        if (savedInstanceState == null) {
            android.support.v4.app.FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            fragment1 = FragmentMacLeanBJ.newInstance("BJ", "MacLean");
            ft.replace(R.id.fragment, fragment1);
            ft.commit();
        }

        //******************************************************************************************
        //
        // Have the app change the fragment based on which element from the names array is chosen.
        //******************************************************************************************

        final ListView lv = (ListView) findViewById(R.id.listViewNames);
        lv.setClickable(true);
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {

                Object o = lv.getItemAtPosition(position);

                //**********************************************************************************
                // Pop a toast
                //**********************************************************************************

                String chosen = (String) o;

                Toast.makeText(getApplicationContext(),
                        chosen, Toast.LENGTH_SHORT).show();

                //**********************************************************************************
                // Log.d example.  This debugging can be viewed in the Logcat tab in Android Studio.
                // A way to allow you to debug your programs.
                //**********************************************************************************
                Log.d("BJTEST", "chosen item from listview = " + chosen);

                Fragment newFragment = new FragmentMacLeanBJ();

                //**********************************************************************************
                // Use a switch to set the appropriate fragment based on the name chosen in the listview.
                //**********************************************************************************

                switch (chosen) {
                    case "Show Dialog":
                        showDialog(MainActivity.this, "Hi", "Did this work?");
                        break;
                    case "Arman":
                        newFragment = FragmentAlfonsoArman3.newInstance("BJ", "MacLean"); //Replace this with your fragment class
                        break;
                    case "Arvind":
                        newFragment = cis2250.hccis.info.fragmentapp.FragmentChauhanArvind.newInstance("BJ", "MacLean"); //Replace this with your fragment class
                        break;
                    case "Ashleigh":
                        newFragment = FragmentPierreAshleigh.newInstance("BJ", "MacLean"); //Replace this with your fragment class
                        break;
                    case "AshleyB":
                        newFragment = FragmentBlanchardAshley.newInstance("BJ", "MacLean"); //Replace this with your fragment class
                        break;
                    case "AshleyJ":
                        newFragment = FragmentJohnstonAshley.newInstance("BJ", "MacLean"); //Replace this with your fragment class
                        break;
                    case "BJ":
                        newFragment = FragmentMacLeanBJ.newInstance("BJ", "MacLean"); //Replace this with your fragment class
                        break;
                    case "Bonnie":
                        newFragment = FragmentChandlerBonnie.newInstance("BJ", "MacLean"); //Replace this with your fragment class
                        break;
                    case "Brandon":
                        newFragment = cis2250.hccis.info.fragmentapp.FragmentAndersonBrandon.newInstance("BJ", "MacLean"); //Replace this with your fragment class
                        break;
                    case "DarcyB":
                        newFragment = cis2250.hccis.info.fragmentapp.FragmentBrownDarcy.newInstance("test", "test");
                        break;
                    case "DarcyW":
                        newFragment = FragmentWattsDarcy.newInstance("test", "test");
                        break;
                    case "Cody":
                        newFragment = cis2250.hccis.info.fragmentapp.FragmentCody.newInstance("BJ", "MacLean"); //Replace this with your fragment class
                        break;
                    case "David":
                        newFragment = cis2250.hccis.info.fragmentapp.FragmentHawkesDavid.newInstance("BJ", "MacLean"); //Replace this with your fragment class
                        break;
                    case "Isaiah":
                        newFragment = new FragmentMacNeillIsaiah();
                        break;
                    case "Jason":
                        newFragment = FragmentCurranJason.newInstance("test", "test");
                        break;
                    case "Jennifer":
                        newFragment = FragmentTownsendJ.newInstance("test", "test");
                        break;
                    case "Nic":
                        newFragment = FragmentPeconiNick.newInstance("test", "test");
                        break;
                    case "Evan":
                        newFragment = FragmentMorrisonEvan.newInstance("BJ", "MacLean"); //Replace this with your fragment class
                        break;
                    case "John":
                        newFragment = FragmentGaudetJohn.newInstance("BJ", "MacLean"); //Replace this with your fragment class
                        break;
                    case "Matt":
                        newFragment = FragmentBryanMatt.newInstance("BJ", "MacLean"); //Replace this with your fragment class
                        break;
                    case "Max":
                        newFragment = new MacDonald_Max_Fragment(); //Replace this with your fragment class
                        break;
                    case "Meric":
                        newFragment = FragmentAkmelMeric.newInstance("test", "test");
                        break;
                    case "Noel":
                        newFragment = cis2250.hccis.info.fragmentapp.FragmentChiassonNoel.newInstance("BJ", "MacLean"); //Replace this with your fragment class
                        break;
                    case "Rodrigo":
                        newFragment = FragmentPiresR.newInstance("BJ", "MacLean"); //Replace this with your fragment class
                        break;
                    case "Ryan":
                        newFragment = cis2250.hccis.info.fragmentapp.FragmentLewisRyan.newInstance("BJ", "MacLean"); //Replace this with your fragment class
                        break;
                    case "Shanny":
                        newFragment = FragmentMinnisShan.newInstance("test", "test");
                        break;
                    case "Sara":
                        newFragment = FragmentCourtneyS.newInstance("BJ", "MacLean"); //Replace this with your fragment class
                        break;
                    case "Sinem":
                        newFragment = FragmentOrakMusluSinem.newInstance("BJ", "MacLean"); //Replace this with your fragment class
                        break;
                    case "Steve":
                        newFragment = FragmentMartinSteve.newInstance("BJ", "MacLean"); //Replace this with your fragment class
                        break;
                    case "Taeju":
                        newFragment = new FragmentHwangTaeju(); //Replace this with your fragment class
                        break;
                    case "Zach":
                        newFragment = new FragmentLundZach(); //Replace this with your fragment class
                        break;
                    default:
                        newFragment = FragmentMacLeanBJ.newInstance("Default", "Default"); //BJs for default
                }

                //**********************************************************************************
                // This code will replace the fragment on the content_main.xml with the new one.
                //**********************************************************************************
                android.support.v4.app.FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                // Replace the contents of the container with the new fragment
                ft.replace(R.id.fragment, newFragment);
                ft.commit();
            }
        });
    }

    /**
     * This method can be used to show a dialog in your activity.  This will allow the user to
     * enter a positive or negative response.
     *
     * @param activity
     * @param title
     * @param message
     * @author BJM/CIS2250
     * @since 20180116
     */

    public void showDialog(Activity activity, String title, CharSequence message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);

        if (title != null) builder.setTitle(title);

        builder.setMessage(message);
        builder.setPositiveButton("Sure", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                Toast.makeText(getApplicationContext(), "selected-positive", Toast.LENGTH_LONG).show();
            }
        });
        builder.setNegativeButton("Not Really", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                Toast.makeText(getApplicationContext(), "selected-negative", Toast.LENGTH_LONG).show();
            }
        });
        builder.show();
    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            //**************************************************************************************
            // make a toast indicating that a nav bar item was chosen
            //**************************************************************************************
            Toast.makeText(getApplicationContext(),
                    "Hi, settings was chosen", Toast.LENGTH_SHORT).show();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_camera) {
            //nav camera
        } else if (id == R.id.nav_gallery) {
            //**************************************************************************************
            // make a toast indicating that a nav bar item was chosen
            //**************************************************************************************

            Toast.makeText(getApplicationContext(),
                    "hi, nav gallery was chosen.", Toast.LENGTH_SHORT).show();
            // Handle the camera action

        } else if (id == R.id.nav_slideshow) {

        } else if (id == R.id.nav_manage) {

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


    @Override
    public void onFragmentInteraction(String test) {

    }

    @Override
    public void onFragmentInteraction(int date) {

    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }


}
