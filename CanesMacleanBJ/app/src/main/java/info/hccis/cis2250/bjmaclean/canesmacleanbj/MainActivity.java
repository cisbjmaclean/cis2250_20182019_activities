package info.hccis.cis2250.bjmaclean.canesmacleanbj;

import android.Manifest;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.icu.util.Calendar;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.CalendarContract;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import info.hccis.cis2250.bjmaclean.canesmacleanbj.fragment.FragmentCamperDetails;
import info.hccis.cis2250.bjmaclean.canesmacleanbj.fragment.FragmentCampers;
import info.hccis.cis2250.bjmaclean.canesmacleanbj.fragment.FragmentHome;
import info.hccis.cis2250.bjmaclean.canesmacleanbj.fragment.FragmentLogin;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener,
        FragmentLogin.OnFragmentInteractionListener,
        FragmentCampers.OnFragmentInteractionListener,
        FragmentCamperDetails.OnFragmentInteractionListener,
        FragmentHome.OnFragmentInteractionListener {

    private static final int MY_PERMISSIONS_REQUEST_READ_WRITE_CALENDAR = 456;

    FragmentLogin fragmentLogin;
    private static boolean loggedInAttribute = false;

    public static boolean getLoggedIn() {
        return loggedInAttribute;
    }

    public static void setLoggedIn(boolean loggedIn) {
        loggedInAttribute = loggedIn;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

//        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
//        fab.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();
//            }
//        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);


    }

    @Override
    protected void onStart() {
        super.onStart();

        //Check to see if the user has logged in.
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        String lastUsername = pref.getString("username", "");
        Fragment firstFragment = null;

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        // get menu from navigationView
        Menu menu = navigationView.getMenu();

        if (lastUsername.equals("")) {
            Log.d("bjtest", "User has not logged in before");
            setLoggedIn(false);
            // find MenuItem you want to change
            firstFragment = new FragmentLogin();
            MenuItem menuItemCampers = menu.findItem(R.id.nav_campers);
            menuItemCampers.setVisible(false);
            MenuItem menuItemLogout = menu.findItem(R.id.nav_logout);
            menuItemLogout.setVisible(false);

        } else {
            setLoggedIn(true);
            MenuItem menuItemLogin = menu.findItem(R.id.nav_login);
            menuItemLogin.setVisible(false);
            firstFragment = FragmentHome.newInstance(lastUsername, "");
        }

        //************************
        //add event
        //************************

        //May not want to request this and then use if right away...
        //Move this to the splash activity if one is added...may not do a splash for Canes.
        ActivityCompat.requestPermissions(MainActivity.this,
                new String[]{Manifest.permission.READ_CALENDAR, Manifest.permission.WRITE_CALENDAR}, MY_PERMISSIONS_REQUEST_READ_WRITE_CALENDAR);

        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        long calID = 4;
        long startMillis = 0;
        long endMillis = 0;
        Calendar beginTime = Calendar.getInstance();

        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
//        Calendar cal = Calendar.getInstance();

        DateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date date = new Date();
        System.out.println(sdf.format(date));
        Log.d("bjest", sdf.format(date)); //2016/11/16 12:08:43
        String fullDate = sdf.format(date);
        String year = fullDate.substring(0, 4);
        String month = fullDate.substring(5, 7);
        String day = fullDate.substring(8, 10);
        String hour = fullDate.substring(11, 13);
        String minute = fullDate.substring(14, 16);

        beginTime.set(Integer.parseInt(year), Integer.parseInt(month) - 1, Integer.parseInt(day), Integer.parseInt(hour), Integer.parseInt(minute));
        startMillis = beginTime.getTimeInMillis();
        Calendar endTime = Calendar.getInstance();
        beginTime.set(Integer.parseInt(year), Integer.parseInt(month) - 1, Integer.parseInt(day), Integer.parseInt(hour), Integer.parseInt(minute) + 1);
        endMillis = endTime.getTimeInMillis();

        ContentResolver cr1 = getContentResolver();
        ContentValues values = new ContentValues();
        values.put(CalendarContract.Events.DTSTART, startMillis);
        values.put(CalendarContract.Events.DTEND, endMillis);
        values.put(CalendarContract.Events.TITLE, "Someone accessed the app");
        values.put(CalendarContract.Events.DESCRIPTION, "Group workout");
        values.put(CalendarContract.Events.CALENDAR_ID, calID);
        values.put(CalendarContract.Events.EVENT_TIMEZONE, "America/Los_Angeles");

        Uri uri1 = null;
        try {

            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_CALENDAR) != PackageManager.PERMISSION_GRANTED) {
                Log.d("bjtest", "More work needed to handle permissions towrite to calendar correctly!");
                
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }
            uri1 = cr1.insert(CalendarContract.Events.CONTENT_URI, values);
            long eventID = Long.parseLong(uri1.getLastPathSegment());
            //get the event ID that is the last element in the Uri
            Log.d("AJ", eventID + "");
            Toast.makeText(this, "Added calendar event" + eventID + "", Toast.LENGTH_LONG).show();
        } catch (Exception e) {
            Log.d("bjtest", "Error creating event: " + e.getMessage());
        }

        //**************************
        //end of content provider
        //**************************


        //Send the user to a login fragment
        android.support.v4.app.FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        // Replace the contents of the container with the new fragment
        ft.replace(R.id.fragment, firstFragment);
        ft.addToBackStack(null).commit();


    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_login) {

            //Send the user to a login fragment
            android.support.v4.app.FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            // Replace the contents of the container with the new fragment
            ft.replace(R.id.fragment, new FragmentLogin());
            ft.addToBackStack(null).commit();


        } else if (id == R.id.nav_campers) {
            android.support.v4.app.FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            // Replace the contents of the container with the new fragment
            ft.replace(R.id.fragment, new FragmentCampers());
            ft.addToBackStack(null).commit();


        } else if (id == R.id.nav_logout) {

            //Clear the username from the SharedPreferences.
            SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
            SharedPreferences.Editor editor = pref.edit();
            editor.putString("username", "");
            editor.commit();

            //Set the logged in flag to false.
            MainActivity.setLoggedIn(false);

            NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
            // get menu from navigationView
            Menu menu = navigationView.getMenu();
            MenuItem menuItemLogout = menu.findItem(R.id.nav_logout);
            menuItemLogout.setVisible(false);
            MenuItem menuItemLogin = menu.findItem(R.id.nav_login);
            menuItemLogin.setVisible(true);
            MenuItem menuItemCampers = menu.findItem(R.id.nav_campers);
            menuItemCampers.setVisible(false);

            //Send the user to a login fragment
            android.support.v4.app.FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            // Replace the contents of the container with the new fragment
            ft.replace(R.id.fragment, new FragmentLogin());
            ft.addToBackStack(null).commit();

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onFragmentInteraction(String test) {
        Log.d("bjtest", "test from activity=" + test);

        //Set the username into the Shared Preferences.

        //If you want to access between activities...
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        SharedPreferences.Editor editor = pref.edit();
        editor.putString("username", test);
        editor.commit();

        MainActivity.setLoggedIn(true);

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        // get menu from navigationView
        Menu menu = navigationView.getMenu();
        MenuItem menuItemLogout = menu.findItem(R.id.nav_logout);
        menuItemLogout.setVisible(true);
        MenuItem menuItemLogin = menu.findItem(R.id.nav_login);
        menuItemLogin.setVisible(false);
        MenuItem menuItemCampers = menu.findItem(R.id.nav_campers);
        menuItemCampers.setVisible(true);

        //Save the username login attempt in a file
        UtilityFile.writeFileOnInternalStorage(getApplicationContext(), "testout.txt", test);
        Log.d("bjtest", "wrote to file");
        Log.d("bjtest read", UtilityFile.read_file(getApplicationContext(), "testout.txt"));

        android.support.v4.app.FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        // Replace the contents of the container with the new fragment
        ft.replace(R.id.fragment, FragmentHome.newInstance(test, ""));
        ft.commit();

    }

    @Override
    public void onFragmentInteraction() {
        Log.d("BJTEST", "This method is implemented in case FragmentHome ever wants to communicate " +
                "with this activity.");
    }

    @Override
    public void onFragmentInteraction(int camperId) {
        Log.d("BJTEST", "This method is implemented in case FragmentCampers ever wants to communicate " +
                "with this activity.");
    }
}
