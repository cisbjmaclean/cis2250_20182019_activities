package info.hccis.cis2250.bjmaclean.canesmacleanbj.dao;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;
import info.hccis.cis2250.bjmaclean.canesmacleanbj.entity.Camper;

@Database(entities = {Camper.class},version = 1)
public abstract class CamperDatabase extends RoomDatabase {
    public abstract CamperDao camperDao();
}
