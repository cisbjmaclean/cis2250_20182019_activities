package info.hccis.cis2250.bjmaclean.canesmacleanbj.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

import info.hccis.cis2250.bjmaclean.canesmacleanbj.entity.Camper;

@Dao
public interface CamperDao {

    @Insert
    public void add(Camper camper);

    @Query("select * from Camper")
    public List<Camper> getCampers();

    @Delete
    public void delete(Camper camper);

    @Update
    public void update(Camper camper);

    @Query("delete from Camper")
    public void deleteAll();

}
