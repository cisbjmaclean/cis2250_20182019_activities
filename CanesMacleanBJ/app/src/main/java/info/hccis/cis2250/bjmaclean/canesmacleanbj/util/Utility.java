package info.hccis.cis2250.bjmaclean.canesmacleanbj.util;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class Utility {

    public static final boolean DEBUGGING = false;

    public static String getHashedPassword(String pw){
                        /*
                Connect to the login service.  For now I have only the hardcoded value of the password.
                 */

        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            byte[] messageDigest = md.digest(pw.getBytes());
            BigInteger bi = new BigInteger(1,messageDigest);
            String hashed = bi.toString(16);
            while(hashed.length()<32){
                hashed = "0"+hashed;
            }
            return hashed;
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * This method can be used to show a dialog in your activity.  This will allow the user to
     * enter a positive or negative response.
     *
     * @param activity
     * @param title
     * @param message
     * @author BJM/CIS2250
     * @since 20180116
     */

    public static void showDialog(final Activity activity, String title, CharSequence message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);

        if (title != null) builder.setTitle(title);

        builder.setMessage(message);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
            }
        });
//        builder.setNegativeButton("Not Really", new DialogInterface.OnClickListener() {
//            public void onClick(DialogInterface dialog, int which) {
//                Toast.makeText(activity.getApplicationContext(), "selected-negative", Toast.LENGTH_LONG).show();
//            }
//        });
        builder.show();
    }

}
