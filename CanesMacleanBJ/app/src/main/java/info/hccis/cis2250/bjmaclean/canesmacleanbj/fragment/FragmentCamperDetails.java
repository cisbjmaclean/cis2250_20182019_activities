package info.hccis.cis2250.bjmaclean.canesmacleanbj.fragment;

import android.content.Context;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import info.hccis.cis2250.bjmaclean.canesmacleanbj.R;
import info.hccis.cis2250.bjmaclean.canesmacleanbj.entity.Camper;


public class FragmentCamperDetails extends Fragment {
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "Camper";

    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    //private Button buttonLogin;
    private EditText editTextLastName;
    private EditText editTextFirstName;
    private EditText editTextDOB;

    public static String username, password, dob;

    public FragmentCamperDetails() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters. test
     *
     * @param camper Parameter 1.
     * @return A new instance of fragment FragmentMacLeanBJ.
     */
    public static FragmentCamperDetails newInstance(Camper camper) {
        FragmentCamperDetails fragment = new FragmentCamperDetails();
        Log.d("bjtest camper details", camper.toString());
        Bundle args = new Bundle();
        args.putSerializable(ARG_PARAM1, camper);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onStart() {
        super.onStart();
        final Bundle tempArgs = getArguments();
        Camper camper = (Camper) tempArgs.getSerializable(ARG_PARAM1);
        Log.d("bjtest in onStart","camper details="+camper.toString());
        //Get references to the buttons.
        //buttonLogin = getView().findViewById(R.id.btn_login);
        editTextLastName = getView().findViewById(R.id.input_camper_last_name);
        editTextFirstName = getView().findViewById(R.id.input_camper_first_name);
        editTextDOB = getView().findViewById(R.id.input_camper_dob);

        editTextFirstName.setText(camper.getFirstName());
        editTextLastName.setText(camper.getLastName());
        editTextDOB.setText(camper.getDob());

        editTextFirstName.setTextColor(Color.BLACK);
        editTextLastName.setTextColor(Color.BLACK);
        editTextDOB.setTextColor(Color.BLACK);

//        buttonLogin.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                //Some functionality needed to authenticate the user...
//                username = editTextEmail.getText().toString();
//
//                password = editTextPassword.getText().toString();
//                Log.d("bjtest", "Password before hash="+password);
//                password = Utility.getHashedPassword(password);  //hash the pw.
//                Log.d("bjtest", "Password hashed"+password);
//                //Login
//                new HttpRequestTask().execute();
//            }
//        });
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View temp = inflater.inflate(R.layout.fragment_camper_detail, container, false);
        return temp;
    }



    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction("test");
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }


    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }






    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(String test);
    }
}
