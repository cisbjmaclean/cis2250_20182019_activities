package info.hccis.cis2250.bjmaclean.canesmacleanbj.fragment;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;

import info.hccis.cis2250.bjmaclean.canesmacleanbj.R;
import info.hccis.cis2250.bjmaclean.canesmacleanbj.entity.User;
import info.hccis.cis2250.bjmaclean.canesmacleanbj.util.Utility;


public class FragmentLogin extends Fragment {
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "firstName";
    private static final String ARG_PARAM2 = "lastName";

    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    private Button buttonLogin;
    private EditText editTextEmail;
    private EditText editTextPassword;

    public static String username, password;

    public FragmentLogin() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters. test
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment FragmentMacLeanBJ.
     */
    public static FragmentLogin newInstance(String param1, String param2) {
        FragmentLogin fragment = new FragmentLogin();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onStart() {
        super.onStart();
        final Bundle tempArgs = getArguments();

        //Get references to the buttons.
        buttonLogin = getView().findViewById(R.id.btn_login);
        editTextEmail = getView().findViewById(R.id.input_email);
        editTextPassword = getView().findViewById(R.id.input_password);

        //Set the username based on the last username that was entered.
        //If you want to access between activities...
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(getView().getContext());
        String lastUsername = pref.getString("username", "");

        editTextEmail.setText(lastUsername);

        buttonLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Some functionality needed to authenticate the user...
                username = editTextEmail.getText().toString();

                password = editTextPassword.getText().toString();
                Log.d("bjtest", "Password before hash="+password);
                password = Utility.getHashedPassword(password);  //hash the pw.
                Log.d("bjtest", "Password hashed"+password);
                //Login
                new HttpRequestTask().execute();
            }
        });
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View temp = inflater.inflate(R.layout.fragment_login, container, false);
        return temp;
    }



    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction("test");
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }


    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    private class HttpRequestTask extends AsyncTask<Void, Void, User> {


        @Override
        protected User doInBackground(Void... params) {
            try {

                /*
                Connect to the login service.  For now I have only the hardcoded value of the password.
                 */




                //Get the service from the strings.xml in res/values
                final String url = getString(R.string.SERVICE_LOGIN)+FragmentLogin.username+"/"+password;

                Log.d("BJTEST", "doInBackground: " + url);
                RestTemplate restTemplate = new RestTemplate();

                MappingJackson2HttpMessageConverter converter = new MappingJackson2HttpMessageConverter();
                MediaType[] mediaTypes = {MediaType.ALL};
                converter.setSupportedMediaTypes(Arrays.asList(mediaTypes));
                restTemplate.getMessageConverters().add(converter);

                User user = null;
                try {
                    user = restTemplate.getForObject(url, User.class);
                    Log.d("bjtest", "found user:"+user.getUserTypeCode());
                } catch (Exception e) {
                    Log.d("bjtest", "Could not login.");
                }

                return user;
            } catch (Exception e) {
                Log.e("bjtest MainActivity", e.getMessage(), e);
            }

            return null;
        }

        /**
         * Check the usertypecode of the user object returned from the service call.  If it is a valid
         * user then the user type code will not be 0.  If valid then communicate with the main activity
         * to switch to the home page.  If not valid then remain on the login fragment.
         * todo: give error message to the user if invalid login
         * @param user
         */

        protected void onPostExecute(User user) {

            if(user != null){
                Log.d("bjtest loginuser =","type="+user.getUserTypeCode()+" Loaded user based on  from rest, ("+user.toString()+" loaded)");
                if(!user.getUserTypeCode().equals("0")) {
                    Log.d("bjtest", "trying to switch fragments");
                    mListener.onFragmentInteraction(user.getFirstName());
                }else{
                    Utility.showDialog(getActivity(), "Login Error", "Invalid Login, try again...");
                }

            }else{
                Utility.showDialog(getActivity(), "Login Error", "Server Unavailable");
            }


        }



    }





    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(String test);
    }
}
