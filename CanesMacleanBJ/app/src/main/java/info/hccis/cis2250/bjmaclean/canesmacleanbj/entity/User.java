package info.hccis.cis2250.bjmaclean.canesmacleanbj.entity;

/**
 * Created by Keith Goddard on 2018-01-28.
 */

public class User {
    private int userId;
    private String username;
    private String password;
    private String lastName;
    private String firstName;
    private String userTypeCode;
    private String createdDateTime;

    public int getUserId() {
        return userId;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public String getLastName() {
        return lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastFirst() { return lastName + ", " + firstName; }

    public String getUserTypeCode() {
        return userTypeCode;
    }

    public String getCreatedDateTime() { return createdDateTime; }

    @Override
    public String toString() {
        return "UserId: " + userId + ", Username: " + username;
    }
}
