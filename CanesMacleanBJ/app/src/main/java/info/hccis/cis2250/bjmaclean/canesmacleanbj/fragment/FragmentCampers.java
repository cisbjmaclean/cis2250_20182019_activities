package info.hccis.cis2250.bjmaclean.canesmacleanbj.fragment;


import android.arch.persistence.room.Room;
import android.content.Context;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import info.hccis.cis2250.bjmaclean.canesmacleanbj.R;
import info.hccis.cis2250.bjmaclean.canesmacleanbj.adapter.CamperAdapter;
import info.hccis.cis2250.bjmaclean.canesmacleanbj.dao.CamperDatabase;
import info.hccis.cis2250.bjmaclean.canesmacleanbj.entity.Camper;
import info.hccis.cis2250.bjmaclean.canesmacleanbj.util.Utility;


/**

 */
public class FragmentCampers extends Fragment {
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "firstName";
    private static final String ARG_PARAM2 = "lastName";

    private String mParam1;
    private String mParam2;

    private RecyclerView recyclerView;
    private CamperAdapter camperAdapter;
    private List<Camper> camperList = new ArrayList<Camper>();

    private OnFragmentInteractionListener mListener;

    //camper room database
    public static CamperDatabase camperDatabase;

    public FragmentCampers() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters. test
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment FragmentMacLeanBJ.
     */
    public static FragmentCampers newInstance(String param1, String param2) {
        FragmentCampers fragment = new FragmentCampers();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onStart() {
        super.onStart();
        final Bundle tempArgs = getArguments();

        Log.d("bjtest","finished with the background and continuing.");

        recyclerView = getView().findViewById(R.id.recyclerViewCampers);
        camperAdapter = new CamperAdapter(getActivity(), camperList);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL));
        recyclerView.setAdapter(camperAdapter);

        //make sure the database is created...
        camperDatabase = Room.databaseBuilder(getContext(),CamperDatabase.class, "camperdb").allowMainThreadQueries().build();

        //Trigger the background process to get the campers from the service.
        new HttpRequestTask().execute();




    }


    /**
     * This method will connect to the web service and set the list of campers based on what is returned.
     * @since 20190125
     * @author BJM
     */
    private class HttpRequestTask extends AsyncTask<Void, Void, Camper[]> {
        @Override
        protected Camper[] doInBackground(Void... params) {
            try {


                final String url = getString(R.string.SERVICE_CAMPERS);
                Log.d("BJTEST", "doInBackground: " + url);
                RestTemplate restTemplate = new RestTemplate();

                https://stackoverflow.com/questions/44176335/restclientexception-could-not-extract-response-no-suitable-httpmessageconverte

                restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
                Camper[] campers = null;

                try {
                    campers = restTemplate.getForObject(url, Camper[].class);
                } catch (Exception e) {
                    Log.w("bjtest", "error getting campers.");
                }

                return campers;
            } catch (Exception e) {
                Log.e("MainActivity", e.getMessage(), e);
            }

            return null;
        }

        protected void onPostExecute(Camper[] campers) {

            //****************************************************************************
            // Refresh the database
            //****************************************************************************

            //todo what if there are no campers in the database but we did connect??

            if(campers != null && campers.length>0){

                //Load the list from the array
                camperList.clear();
                camperList.addAll(Arrays.asList(campers));

                //test
                Log.d("bjtest", "size from database="+FragmentCampers.camperDatabase.camperDao().getCampers().size());
                ArrayList<Camper> theCampers = new ArrayList<Camper>(FragmentCampers.camperDatabase.camperDao().getCampers());

                if(Utility.DEBUGGING) Utility.showDialog(getActivity(), "test", ""+theCampers.size());

                //delete
                FragmentCampers.camperDatabase.camperDao().deleteAll();

                //insert all
                for(Camper camper: campers) {
                    FragmentCampers.camperDatabase.camperDao().add(camper);
                }
            }else{ //could not connect so get from the room database.

                Log.d("bjtest","loaded from the Room db");

                ArrayList<Camper> theCampers = new ArrayList<Camper>(FragmentCampers.camperDatabase.camperDao().getCampers());
                if(Utility.DEBUGGING) Utility.showDialog(getActivity(), "test", "loaded from "+theCampers.size());

                camperList.clear();
                camperList.addAll(theCampers);

            }


            Log.d("bjtest load campers","Loaded campers from rest, ("+camperList.size()+" campers loaded)");
            camperAdapter.notifyDataSetChanged();
        }
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_campers, container, false);

        return view;
    }

    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(0);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(int camperId);
    }
}
