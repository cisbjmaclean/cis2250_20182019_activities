package info.hccis.cis2250.bjmaclean.canesmacleanbj.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import info.hccis.cis2250.bjmaclean.canesmacleanbj.R;
import info.hccis.cis2250.bjmaclean.canesmacleanbj.entity.Camper;
import info.hccis.cis2250.bjmaclean.canesmacleanbj.fragment.FragmentCamperDetails;
import info.hccis.cis2250.bjmaclean.canesmacleanbj.util.Utility;

/**
 * Created by kgoddard on 2/2/2018.
 */

public class CamperAdapter extends RecyclerView.Adapter<CamperAdapter.MyViewHolder> {
    private List<Camper> campers;
    private Context context;

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.camper_row, parent, false);
        Log.d("KGADAPTER", "onCreateViewHolder");
        return new MyViewHolder(itemView);
    }

    public CamperAdapter(Context context, List<Camper> camperList) {
        Log.d("KGADAPTER bjtest", "Instantiating adapter");
        this.context = context;
        this.campers = camperList;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        final Camper camper = campers.get(position);
        holder.camperFirstName.setText(camper.getFirstName());
        holder.camperLastName.setText(camper.getLastName());

    }

    @Override
    public int getItemCount() {
        return campers.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView camperFirstName, camperLastName;
        //public ImageButton expand;

        public MyViewHolder(View view) {
            super(view);
            camperFirstName = view.findViewById(R.id.textViewCamperFirstName);
            camperLastName = view.findViewById(R.id.textViewCamperLastName);

            view.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    // TODO Auto-generated method stub
                    Utility.showDialog((Activity)v.getContext(), "Camper Details", campers.get(getAdapterPosition()).toString());
                    return true;
                }
            });

            //Once we click this row we will want to show more details about this camper.
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.d("bjtest", "clicked row in list of campers..."+getAdapterPosition());
                    Log.d("bjtest", "Camper details:..."+campers.get(getAdapterPosition()).toString());


                    //used this resource for the following syntax:  http://code.i-harness.com/en/q/1ba462f
                    AppCompatActivity activity = (AppCompatActivity) v.getContext();

                    /* Note that I am passing the camper object based on the position that was clicked
                    in the list.  I had to modify the newInstance method to accept a camper object.  then I use
                    the code to set a serializable object in the bundle which is passed to the onStart of
                    the fragment.  This allows the camper object to get from here to the details fragment.
                     */

                    activity.getSupportFragmentManager().beginTransaction().replace(R.id.fragment,
                            FragmentCamperDetails.newInstance(campers.get(getAdapterPosition()))).addToBackStack(null).commit();

                }
            });


        }

        @Override
        public void onClick(View v) {
            Toast.makeText(v.getContext(), "clicked", Toast.LENGTH_LONG).show();
        }
    }

}
