package info.hccis.cis2250.waterbm;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    Button buttonCalculate;
    EditText editTextDiameter;
    EditText editTextHeight;
    EditText editTextFlowRate;
    TextView textViewResult;

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        Log.d("bjtest","about to put  the instance state back in");
        super.onSaveInstanceState(outState);
        Log.d("bjtest","putting the instance state back in");
    }

    public void initialize() {
        buttonCalculate = findViewById(R.id.buttonCalculate);
        editTextDiameter = findViewById(R.id.editTextDiameter);
        editTextHeight = findViewById(R.id.editTextHeight);
        editTextFlowRate = findViewById(R.id.editTextFlowRate);
        textViewResult = findViewById(R.id.textViewResult);

        //Read from the last time the information was saved to Shared Preferences
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());

        float defaultValue = 0.0f; //if no last height saved
        float lastHeight = sharedPref.getFloat(getString(R.string.label_height), defaultValue);


        Log.d("bjtest-lastHeight","lastHeight="+lastHeight); //debug statement to LogCat
        editTextHeight.setText(String.valueOf(lastHeight));  //set the initial value on height

        buttonCalculate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String diameter = editTextDiameter.getText().toString();
                String height = editTextHeight.getText().toString();
                String flowRate = editTextFlowRate.getText().toString();

                Calculator calculator=null;
                try {

                    calculator= new Calculator(Double.parseDouble(diameter),
                            Double.parseDouble(height),
                            Double.parseDouble(flowRate));
                    Log.d("BJMDEBUG", "Time to fill=" + calculator.getFillTimeForContainer());

                }catch(Exception e){
                    calculator = new Calculator();
                    Log.w("bjtest", "error calculating");
                }
                textViewResult.setText(String.valueOf(calculator.getFillTimeForContainer()));

                //Save the last data to the shared preferences
                //SharedPreferences sharedPref = MainActivity.this.getPreferences(Context.MODE_PRIVATE);

                //If you want to access between activities...
                SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());

                SharedPreferences.Editor editor = pref.edit();
                editor.putFloat(getString(R.string.label_height), Float.parseFloat(height));
                editor.commit();


            }
        });
    }

    @Override
    protected void onPause() {
        super.onPause();

        //save instance state



    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Bundle resetInstanceState = new Bundle();
        super.onCreate(resetInstanceState);
        Log.d("bjtest", "onCreate is triggered");
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
        initialize();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
