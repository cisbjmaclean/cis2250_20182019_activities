package info.hccis.cis2250.waterbm;

import java.util.Scanner;
/**
 *
 * @since Jan 8, 2018
 * @author bjmaclean
 */
public class Calculator {
    private double diameter, height, flowRate, volume, fillTimeForContainer;
    private static Scanner input = new Scanner(System.in);
    public Calculator(double diameter, double height, double flowRate) {
        this.diameter = diameter;
        this.height = height;
        this.flowRate = flowRate;
        calculate();
    }
    //Put back default constructor
    public Calculator() {
    }
    

    
    /**
     * This method will set the diameter, height and flow rate based on 
     * user input.
     * 
     * @since Jan 8, 2018
     * @author bjmaclean
     */
    public void getInput() {
        setDiameter();
        setHeight();
        setFlowRate();
    }
    public double getDiameter() {
        return diameter;
    }
    public void setDiameter(double diameter) {
        this.diameter = diameter;
    }
    /**
     * This method set the diameter based on user input
     *
     * @since 20180108
     * @author CIS1232A
     */
    public void setDiameter() {
        System.out.println("Enter diameter");
        this.diameter = input.nextDouble();
        input.nextLine(); //burn the line
        calculate();
    }
    public double getHeight() {
        return height;
    }
    public void setHeight(double height) {
        this.height = height;
    }
    /**
     * This method set the height based on user input
     *
     * @since 20180108
     * @author CIS1232A
     */
    public void setHeight() {
        System.out.println("Enter height");
        this.height = input.nextDouble();
        input.nextLine(); //burn the line
        calculate();
    }
    public double getFlowRate() {
        return flowRate;
    }
    public void setFlowRate(double flowRate) {
        this.flowRate = flowRate;
    }
    /**
     * This method set the flow rate based on user input
     *
     * @since 20180108
     * @author CIS1232A
     */
    public void setFlowRate() {
        System.out.println("Enter flow rate");
        this.flowRate = input.nextDouble();
        input.nextLine(); //burn the line
        calculate();
    }
    public void calculate(){
        this.calculateVolume();
        this.calculateTimeToFill();
    }
    
    /**
     * Calculate volume as pi * radius * radius * height
     *
     * @since 20180108
     * @author CIS1232A
     */
    public void calculateVolume() {
        volume = Math.pow((diameter / 2.0), 2) * Math.PI * height;
    }
    /**
     * This will calculate time to fill the specific volume
     *
     * @since 20180108
     * @author CIS1232A
     */
    public void calculateTimeToFill() {
        //Time is equal to the volume * flowrate / 61.
        //61 is the number of cubic inches in a liter
        fillTimeForContainer = volume * (flowRate / 61.0);
    }
    public double getVolume() {
        return this.volume;
    }
    public double getFillTimeForContainer() {
        return fillTimeForContainer;
    }
    /**
     * This method will show the details to the console.
     *
     * @since 20180108
     * @author CIS1232A
     */
    public void display() {
        System.out.println(this.toString());
    }
    public String toString() {
        return "Time=" + fillTimeForContainer;
    }
}